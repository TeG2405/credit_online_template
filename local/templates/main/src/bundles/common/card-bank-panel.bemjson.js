module.exports = [
    {block: 'card-bank', content: [
        {cls: 'row', content: [
            {elem: 'wrap', cls: 'col-12 col-md-3 col-lg-12 text-lg-left mb-lg-0', content: [
                {cls: 'row align-items-center', content: [
                    {cls: 'col-6 col-sm-4 col-md-12', content: [
                        {elem: 'image', content: [
                            {block: 'a', cls: 'd-block', content: [
                                {block: 'image', mods: {size: '140x50'}},
                            ]},
                        ]},
                    ]},
                    {elem: 'rating', cls: 'col-6 col-sm-8 col-md-12 text-lg-left', content: [
                        {block: 'rating'},
                    ]},
                ]},
            ]},
            {cls: 'col-12 col-md-9 col-lg-12', content: [
                {elem: 'body', content: [
                    {cls: 'row', content: [
                        {cls: 'col-12  col-sm-4 col-lg-12 mb-lg-2', content: [
                            {elem: 'feature', content: 'Лицензия:  №1000'},
                            {elem: 'feature', content: [
                                {block: 'a', content: 'Отзывы:  2345'},
                            ]},
                        ]},
                        {cls: 'col-12 col-sm-8 col-lg-12', content: [
                            {elem: 'feature', content: [
                                {block: 'fi', cls: 'mr-1 text-primary', mods: {icon: 'call'}},
                                {block: 'a', attrs: {href: 'tel:88001002424'}, content: '8 (800)100-24-24'},
                            ]},
                            {elem: 'feature', content: [
                                {block: 'fi', cls: 'mr-1 text-primary', mods: {icon: 'mark'}},
                                {tag: 'span', content: '109147, г. Москва, ул. Воронцовская, д. 43'},
                            ]},
                        ]},
                    ]},
                ]},
                {block: 'h', size: '4', cls: 'my-2', content: 'Предложения'},
                {elem: 'list', content: ['Вклады', 'Кредиты', 'Кредитные карты', 'Ипотека', 'Кредиты для бизнеса'].map((item) => [
                    {elem: 'li', content: [
                        {elem: 'btn', cls: 'btn btn-blue', content: [
                            item,
                            {tag: 'span', cls: 'ml-1', content: Math.floor(Math.random() * 100)},
                        ]},
                    ]},
                ])},
            ]},
        ]},
    ]},
];
