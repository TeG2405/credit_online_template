module.exports = function(bh) {
    bh.match('progress-ring', function(ctx, json) {
        var radius = json.size/2 - json.width;
        var circumference = radius * 2 * Math.PI;
        var percent = 1 - json.percent || Math.random();
        ctx.content(`
        <svg class="progress-ring__icon" height="${json.size}" width="${json.size}">
             <circle class="progress-ring__circle" 
            fill="transparent" 
            stroke-width="${json.width}" 
            r="${radius}" 
            cx="${json.size/2}" 
            cy="${json.size/2}" />
            <circle class="progress-ring__circle" 
            fill="transparent" 
            stroke-dasharray="${circumference} ${circumference}"
            stroke-dashoffset="${circumference * percent}"
            stroke-width="${json.width}" 
            r="${radius}" 
            cx="${json.size/2}" 
            cy="${json.size/2}" />
        </svg>
        `);
    });
};
