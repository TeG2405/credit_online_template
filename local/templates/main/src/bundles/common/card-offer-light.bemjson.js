module.exports = [
    {block: 'card-offer', content: [
        {cls: 'row', content: [
            {elem: 'wrap', cls: 'col-12 col-xl-3', content: [
                {cls: 'row align-items-center', content: [
                    {cls: 'col-6 col-sm-4 col-xl-12', content: [
                        {elem: 'image', content: [
                            {block: 'a', cls: 'd-block', content: [
                                {block: 'image', mods: {size: '140x50'}},
                            ]},
                        ]},
                    ]},
                    {elem: 'rating', cls: 'col-6 col-sm-8 col-xl-12', content: [
                        {block: 'rating'},
                    ]},
                ]},
            ]},
            {cls: 'col-12 col-xl-9', content: [
                {elem: 'body', content: [
                    {cls: 'row', content: [
                        {cls: 'col-12 mb-2 d-sm-none', content: [
                            {elem: 'label', content: [
                                {block: 'fi', mods: {icon: 'like'}},
                                {content: '<b>95%</b> одобрений'},
                            ]},
                        ]},
                        {cls: 'col-12 col-sm-4 col-xl-3 mb-2 mb-sm-0', content: [
                            {cls: 'row', content: [
                                {elem: 'group', cls: 'col-6 col-sm-12', content: [
                                    {cls: 'font-size-small', content: [
                                        'Сумма кредита',
                                    ]},
                                    {cls: 'text-primary font-weight-normal', content: [
                                        'до 700 000 руб.',
                                    ]},
                                ]},
                                {elem: 'group', cls: 'col-6 col-sm-12', content: [
                                    {cls: 'font-size-small', content: [
                                        'Срок кредита',
                                    ]},
                                    {cls: 'text-primary font-weight-normal', content: [
                                        'до 100 лет',
                                    ]},
                                ]},
                            ]},
                        ]},
                        {cls: 'col-12 col-sm-8 col-xl-6', content: [
                            {cls: 'w-100 font-size-small', content: [
                                {cls: 'row', content: [
                                    {cls: 'col-6 font-weight-normal pr-0', content: 'Ставка'},
                                    {cls: 'col-6', content: 'от 11.9 %'},
                                ]},
                                {cls: 'row', content: [
                                    {cls: 'col-6 font-weight-normal pr-0', content: 'Возраст'},
                                    {cls: 'col-6', content: 'от 100 лет'},
                                ]},
                                {cls: 'row', content: [
                                    {cls: 'col-6 font-weight-normal pr-0', content: 'Рассмотрение'},
                                    {cls: 'col-6', content: '100 лет'},
                                ]},
                                {cls: 'row', content: [
                                    {cls: 'col-6 font-weight-normal pr-0', content: 'Поручительство'},
                                    {cls: 'col-6', content: 'без поручителей'},
                                ]},
                                {cls: 'row', content: [
                                    {cls: 'col-6 font-weight-normal pr-0', content: 'Документы'},
                                    {cls: 'col-6', content: 'только паспорт'},
                                ]},
                            ]},
                        ]},
                        {cls: 'col-12 col-xl-3 mt-2 mt-xl-0', content: [
                            {cls: 'row', content: [
                                {cls: 'col-6 col-sm-4 col-md-3 col-xl-12', content: [
                                    {block: 'btn', cls: 'btn-block btn-primary btn-sm', content: 'Оформить'},
                                ]},
                                {cls: 'col-12 col-sm-4 col-md-4 col-xl-12 d-none d-sm-block mt-xl-2', content: [
                                    {elem: 'label', content: [
                                        {block: 'fi', mods: {icon: 'like'}},
                                        {content: '<b>95%</b> одобрений'},
                                    ]},
                                ]},
                                {cls: 'col-6 col-sm-4 col-md-5 d-xl-none', content: [
                                    {elem: 'controls', cls: 'text-right', content: [
                                        {block: 'a', cls: 'active', content: [
                                            {block: 'fi', mods: {icon: 'heart-o'}},
                                        ]},
                                        {block: 'a', content: [
                                            {block: 'fi', mods: {icon: 'layers'}},
                                        ]},
                                    ]},
                                ]},
                            ]},
                        ]},
                    ]},
                ]},
            ]},
        ]},
    ]},
];
