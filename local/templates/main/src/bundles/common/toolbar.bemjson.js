module.exports = [
    {block: 'toolbar', content: [
        {elem: 'row', content: [
            {elem: 'col', content: [
                {block: 'nav-simple', content: [
                    {elem: 'item', cls: 'dropdown', content: [
                        {elem: 'link', attrs: {'data-toggle': 'modal', 'href': '#MODAL_CITY_CHANGE'}, content: [
                            {elem: 'icon', mix: {block: 'fi', mods: {icon: 'mark'}}},
                            {elem: 'text', content: 'Волгоград'},
                        ]},
                        {block: 'ml-2', mix: {block: 'dropdown-menu'}, cls: 'font-size-small px-2 show', content: [
                            {cls: 'mb-s', content: 'Это ваш город?'},
                            {block: 'btn', cls: 'btn-sm btn-primary py-x mr-1', attrs: {onclick: `this.parentNode.remove()`}, content: 'Да'},
                            {block: 'btn', cls: 'btn-sm btn-blue py-x', attrs: {'data-toggle': 'modal', 'data-target': '#MODAL_CITY_CHANGE'}, content: 'Нет'},
                        ]},
                    ]},
                    {elem: 'item', content: [
                        {elem: 'link', attrs: {href: 'tel:+78009009090'}, content: [
                            {elem: 'icon', mix: {block: 'fi', mods: {icon: 'call'}}},
                            {elem: 'text', content: '8 (800) 900 90 90'},
                        ]},
                    ]},
                ]},
            ]},
            {elem: 'col', cls: 'd-none d-lg-block', content: [
                {elem: 'row', content: [
                    {elem: 'col', cls: 'py-0', content: [
                        {block: 'btn', cls: 'btn-secondary btn-sm px-lg-3', content: 'Онлайн-заявки во все банки'},
                    ]},
                    {elem: 'col', cls: 'py-0', content: [
                        {block: 'btn', cls: 'btn-primary btn-sm px-2', content: [
                            {elem: 'pulsar'},
                            'Подбор кредита',
                        ]},
                    ]},
                ]},
            ]},
            {elem: 'col', content: [
                {block: 'nav-simple', content: [
                    {elem: 'item', cls: 'd-none d-xl-inline-block', content: [
                        {elem: 'link', content: [
                            {elem: 'icon', mix: {block: 'fi', mods: {icon: 'heart-o'}}, attrs: {'data-count': '3'}},
                            {elem: 'text', content: 'Избранное'},
                        ]},
                    ]},
                    {elem: 'item', cls: 'd-none d-xl-inline-block', content: [
                        {elem: 'link', content: [
                            {elem: 'icon', mix: {block: 'fi', mods: {icon: 'layers'}}, attrs: {'data-count': '2'}},
                            {elem: 'text', content: 'Сравнить'},
                        ]},
                    ]},
                    {elem: 'item', cls: 'd-none d-xl-inline-block', content: [
                        {elem: 'link', attrs: {'data-toggle': 'modal', 'href': '#MODAL_AUTHORIZATION'}, content: [
                            {elem: 'icon', mix: {block: 'fi', mods: {icon: 'user'}}},
                            {elem: 'text', content: 'Вход'},
                        ]},
                    ]},
                    {elem: 'item', cls: 'dropdown d-xl-none', content: [
                        {elem: 'link', tag: 'button', attrs: {'data-toggle': 'dropdown'}, content: [
                            {elem: 'icon', mix: {block: 'fi', mods: {icon: 'user'}}},
                            {elem: 'text', content: 'Имя пользователя'},
                        ]},
                        {block: 'dropdown-menu', cls: 'dropdown-menu-right font-size-small', content: [
                            'Профиль',
                            'История заказов',
                            '...',
                            'Пункт личного кабинета N',
                        ]},
                    ]},
                ]},
            ]},
        ]},
    ]},
];
