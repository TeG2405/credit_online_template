(($)=>{
    $('[data-toggle="agreement"]').on('change', (e)=>{
        e.target.checked = !e.target.checked;
        e.preventDefault();
    });
})($);

