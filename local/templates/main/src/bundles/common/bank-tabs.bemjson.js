const ITEMS = [
    ['Кредит наличными', 'hand-money.svg'],
    ['Микрозайм', 'hand-coins.svg'],
    ['Кредит для бизнеса', 'briefcase.svg'],
    ['Кредитные карты', 'credit-card.svg'],
    ['Автокредит', 'electric-car.svg'],
    ['Курс валют', 'coins.svg'],
    ['Дебетовые карты', 'debit-card.svg'],
    ['Заявка в банки', 'bank.svg'],
    ['Заявка в МФО', 'message.svg'],
    ['Ипотека', 'mortgage.svg'],
    ['Вклады', 'piggy-bank.svg'],
];
module.exports = [
    {block: 'nav', mods: {type: 'simple'}, cls: 'mb-2', content: [
        {elem: 'item', cls: 'nav-item', content: [
            {block: 'a', cls: 'nav-link active', attrs: {'data-toggle': 'tab', 'href': '#BANK_DETAIL_TAB_DESCRIPTION'}, content: [
                {tag: 'span', content: 'Описание'},
            ]},
        ]},
        {elem: 'item', cls: 'nav-item', content: [
            {block: 'a', cls: 'nav-link', attrs: {'data-toggle': 'tab', 'href': '#BANK_DETAIL_TAB_REVIEWS'}, content: [
                {tag: 'span', content: 'Отзывы'},
            ]},
        ]},
    ]},
    {block: 'tab-content', cls: 'mb-3', content: [
        // Секция описания
        {mix: {block: 'tab-pane'}, cls: 'active show', attrs: {id: 'BANK_DETAIL_TAB_DESCRIPTION'}, content: [
            {block: 'title-compound', cls: 'justify-content-between align-items-center', content: [
                {elem: 'header', tag: 'h3', content: 'Предложения'},
                {elem: 'addition', cls: 'w-100 w-sm-auto mt-2 mt-sm-0', content: [
                    {block: 'nav', tag: 'nav', mods: {type: 'toggle'}, content: [
                        {block: 'a', attrs: {'data-toggle': 'tab', 'href': '#BANK_DETAIL_TAB_TEASER'}, cls: 'nav-item btn btn-blue w-100 w-sm-auto  active show', content: [
                            {block: 'fi', cls: 'align-middle mr-1', mods: {icon: 'align-left'}},
                            {tag: 'span', cls: 'align-middle', content: 'Кратко'},
                        ]},
                        {block: 'a', attrs: {'data-toggle': 'tab', 'href': '#BANK_DETAIL_TAB_DETAIL'}, cls: 'nav-item btn btn-blue w-100 w-sm-auto', content: [
                            {block: 'fi', cls: 'align-middle mr-1', mods: {icon: 'align-justify'}},
                            {tag: 'span', cls: 'align-middle', content: 'Подробно'},
                        ]},
                    ]},
                ]},
            ]},
            // Табы описания
            {block: 'tab-content', content: [
                {mix: {block: 'tab-pane'}, cls: 'active show', attrs: {id: 'BANK_DETAIL_TAB_TEASER'}, content: [
                    {cls: 'bg-blue-light p-3', content: [
                        require('./swiper-row-offer-teaser.bemjson'),
                    ]},
                ]},
                {mix: {block: 'tab-pane'}, cls: '', attrs: {id: 'BANK_DETAIL_TAB_DETAIL'}, content: [
                    {cls: 'bg-blue-light p-3', content: [
                        ITEMS.map((item) => [
                            {block: 'h', size: '6', content: item[0]},
                            {block: 'list-columns', cls: 'font-size-md', content: ['Выгодный', 'Комфортный онлайн', 'Комфортный', 'Пополняемый онлайн', 'Ипотечный бонус', 'Наличными', 'Выгодный', 'Комфортный онлайн', 'Комфортный', 'Пополняемый онлайн', 'Ипотечный бонус', 'Наличными'].map((item) => [
                                {elem: 'item', content: [
                                    {block: 'a', content: item},
                                ]},
                            ])},
                        ]),
                    ]},
                ]},
            ]},
            // Блок Описания
            {block: 'mt-3', content: [
                require('./card-bank-info.bemjson'),
            ]},
            {block: 'mt-3', content: [
                new Array(2).fill([
                    {tag: 'p', content: 'Кредитная организация была учреждена в 1990 году под наименованием «Банк внешней торговли РФ» (позднее — Внешторгбанк, ныне — ВТБ). До III квартала 2002 года пакетом в 99,9% акций банкавладел ЦБ РФ, который в рамках реформы банковского сектора передал его Министерствуимущественных отношений России. В мае 2007 года банк ВТБ успешно для себя провел IPO. В ходепервичного публичного размещения 17,7% акций купили институциональные российские изарубежные инвесторы, 4,8% — примерно 120 тыс. частных лиц. В сентябре 2009 года проведенодоразмещение, по результатам которого доля государства в уставном капитале достигла 85,5%. Намомент проведения IPO капитализация ВТБ составила 35,5 млрд долларов США. В мае 2013 годаВТБ осуществил допэмиссию акций объемом 102,5 млрд рублей, по итогам которой сообщил оснижении доли государства, владевшего до этого (посредством Федерального агентства поуправлению государственным имуществом) пакетом в 75,5% акций, до 60,9%; остальные акциинаходятся в свободном обращении.'},
                ]),
            ]},
            // Блок: Карусель карты
            {block: 'h', size: '3', content: 'Дизайн пластиковых карт'},
            require('./swiper-row-card.bemjson'),
            // Блок: Новости
            {block: 'h', size: '3', content: 'Новости'},
            new Array(7).fill([
                {block: 'teaser-news', cls: 'mb-2 font-size-md', content: [
                    {elem: 'date', content: '00:00'},
                    {elem: 'title', content: 'СМИ: тричлена ОПЕК наложат вето на предложение об увеличении объемов добычи нефти СМИ: тричлена ОПЕК наложат вето на предложение об увеличении объемов добычи нефти'},
                ]},
            ]),
        ]},
        // Секция отзывов
        {mix: {block: 'tab-pane'}, attrs: {id: 'BANK_DETAIL_TAB_REVIEWS'}, content: [
            {content: [
                new Array(3).fill(require('./review.bemjson')),
                {block: 'my-3', content: [
                    require('./review-tools.bemjson'),
                ]},
                // Блок: постраничная навигация
                require('./swiper-page-pagination.bemjson'),
            ]},
        ]},
    ]},
];
