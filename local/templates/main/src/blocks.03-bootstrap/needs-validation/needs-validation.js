(($)=>{
    Array.prototype.filter.call(document.getElementsByClassName('needs-validation'), (form) => {
        let poppers = Array.from(form.getElementsByClassName('tooltip-form-control')).map((tooltip)=>{
            return new Popper(tooltip.parentElement.getElementsByClassName('form-control')[0], tooltip, {
                placement: 'right-center',
                modifiers: {
                    flip: {
                        behavior: ['bottom-start'],
                    },
                    preventOverflow: {
                        boundariesElement: document.body,
                    },
                },
            });
        });
        form.addEventListener('submit', function(event) {
            if (form.checkValidity() === false) {
                event.preventDefault();
                event.stopPropagation();
            }
            poppers.forEach((popper) => {
                form.contains(popper.reference) && popper.update();
            });
            form.classList.add('was-validated');
        }, false);
    });
})($);
