module.exports = [
    {block: 'nav-simple', cls: 'ml-0 text-primary font-size-small', content: [
        {elem: 'item', cls: 'pl-0', content: [
            {elem: 'link', attrs: {'data-toggle': 'modal', 'href': '#MODAL_CITY_CHANGE'}, content: [
                {elem: 'text', content: 'Изменить город'},
            ]},
        ]},
        {elem: 'item', cls: 'pl-0', content: [
            {elem: 'link', content: [
                {elem: 'text', elemMods: {decoration: 'underline'}, content: 'Россия'},
            ]},
        ]},
    ]},
];
