module.exports = [
    {block: 'swiper-row', cls: 'row', content: [
        {cls: 'w-100', content: [
            {cls: 'swiper-container swiper-container-horizontal', content: [
                {cls: 'swiper-wrapper', content: ((item) => new Array(12).fill(item))([
                    {block: 'col-6', cls: 'col-sm-4 col-xl-3 swiper-slide', content: [
                        {block: 'a', cls: 'd-block', content: [
                            {block: 'image', mods: {size: '160x80'}, cls: 'text-center', content: [
                                {block: 'img', width: 160, height: 80, mods: {lazy: true}, src: 'http://placehold.it/160x80'},
                            ]},
                            {block: 'mt-1', cls: 'font-size-md text-center', content: 'Банк Тинькофф – Кредиты наличными'},
                        ]},
                    ]},
                ])},
                {cls: 'swiper-pagination position-relative mt-3 d-none d-md-block'},
            ]},
        ]},
    ]},
];
