module.exports = [
    {block: 'search', content: [
        {elem: 'input', attrs: {'data-toggle': 'autocomplite', 'data-url': 'stubs/banks.json', 'data-callback': 'stubFrontCallBack'}, placeholder: 'Поиск по сайту'},
        {elem: 'btn', content: [
            {block: 'fi', cls: 'align-middle', mods: {icon: 'search'}},
        ]},
        {elem: 'btn', attrs: {'data-toggle': 'open', 'data-target': '#SEARCH'}, content: [
            {block: 'fi', cls: 'align-middle', mods: {icon: 'close'}},
        ]},
    ]},
];
