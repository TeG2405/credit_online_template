module.exports = [
    {block: 'tile-news', content: [
        {elem: 'col', cls: 'col-12 col-xl-7 h-xl-100', content: require('./card-news-large.bemjson')},
        {elem: 'col', cls: 'col-12 col-xl-5 h-xl-50', content: require('./card-news.bemjson')},
        {elem: 'col', cls: 'col-12 col-xl-5 h-xl-50', content: require('./card-news.bemjson')},
    ]},
];
