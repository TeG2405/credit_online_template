const ClassName = {
    BLOCK: 'header',
    SHOW: 'show',
    FADE: 'fade',
};
const Event = {
    SHOW: 'show.iv.open',
    HIDE: 'hide.iv.open',
};
const Selector = {
    BLOCK: '.header',
    SEARCH: '.header__search',
};
$(document)
    .on(Event.SHOW, Selector.SEARCH, (event)=>{
        event.target.getElementsByClassName('search__input')[0].focus();
    });
