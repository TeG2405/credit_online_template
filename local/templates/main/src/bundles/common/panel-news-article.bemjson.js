module.exports = [
    {block: 'panel', cls: 'h-md-100 bg-light', content: [
        {elem: 'wrap', cls: 'position-md-absolute', content: [
            {elem: 'inner', content: [
                {elem: 'wrap', cls: 'position-md-absolute', content: [
                    {elem: 'body', cls: 'd-none d-md-block', content: [
                        {elem: 'shadow'},
                        {elem: 'scroll', mix: {block: 'custom-scroll-bar'}, cls: 'position-md-absolute', content: [
                            {cls: 'p-3', content: [
                                {block: 'font-size-md', content: [
                                    new Array(25).fill(require('./teaser-news-article.bemjson')),
                                ]},
                            ]},
                        ]},
                        {elem: 'shadow'},
                    ]},
                    {elem: 'footer', cls: 'font-size-md text-center', content: [
                        {block: 'a', content: 'Все статьи'},
                    ]},
                ]},
            ]},
        ]},
    ]},
];
