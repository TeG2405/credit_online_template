module.exports = [
    {block: 'swiper-row', cls: 'row', content: [
        {cls: 'w-100', content: [
            {cls: 'swiper-container swiper-container-horizontal', content: [
                {cls: 'swiper-wrapper', content: ((item) => new Array(12).fill(item))([
                    {block: 'col-6', cls: 'col-sm-4 col-lg-3 swiper-slide', content: [
                        {block: 'image', mods: {size: '170x110'}, cls: 'text-center', content: [
                            {block: 'img', width: 170, height: 110, mods: {lazy: true}, src: 'http://placehold.it/170x110'},
                        ]},
                    ]},
                ])},
                {cls: 'swiper-pagination position-relative mt-2 d-none d-md-block'},
            ]},
        ]},
    ]},
];
