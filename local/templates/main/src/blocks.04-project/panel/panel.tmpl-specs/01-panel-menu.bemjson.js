class PanelMenu {
    constructor(title = 'Заголовок панели', items = new Array(10).fill('Пункт меню'), collapsed = true) {
        this.template = [
            {block: 'panel', cls: 'bg-light', content: [
                {elem: 'wrap', content: [
                    {elem: 'inner', content: [
                        {elem: 'wrap', content: [
                            {elem: 'heading', attrs: {'data-toggle': 'collapse'}, cls: `pb-2 ${collapsed ? ' collapsed' : ''}`, content: [
                                {elem: 'title', content: title},
                            ]},
                            {elem: 'body', cls: 'pb-2', content: [
                                {elem: 'collapse', cls: `collapse ${collapsed ? ' ' : 'show'}`, content: [
                                    {block: 'pb-2', content: [
                                        {block: 'nav', mods: {type: 'dropdown'}, cls: 'flex-column', content: items.map((item)=>[
                                            {elem: 'item', cls: 'nav-item', content: [
                                                {block: 'a', cls: 'nav-link', content: [
                                                    Array.isArray(item) ?
                                                    {cls: 'nav-icon', content: [
                                                        {block: 'image', mods: {size: '38x38', align: 'middle'}, content: [
                                                            {block: 'img', mods: {lazy: true}, src: 'upload/tile/'+item[1]},
                                                            {elem: 'char', cls: 'text-primary', content: item[0][0]},
                                                        ]},
                                                    ]}:
                                                    {block: 'fi', mods: {icon: 'circle-o'}},
                                                    {tag: 'span', cls: 'align-middle', content: Array.isArray(item) ? item[0] : item},
                                                ]},
                                            ]},
                                        ])},
                                    ]},
                                ]},
                            ]},
                        ]},
                    ]},
                ]},
            ]},
        ];
    }
    getTemplate() {
        return this.template;
    }
}
module.exports = (new PanelMenu).getTemplate();
module.exports.new = (title = 'Заголовок панели', items = new Array(10).fill('Пункт меню'), collapsed = true) => {
    return (new PanelMenu(title, items, collapsed)).getTemplate();
};
