import {Util} from 'bootstrap';
(($)=>{
    $('.subscribe').on('change', (e) => {
        $(Util.getSelectorFromElement(e.target))
            .modal('show');
    });
})($);
