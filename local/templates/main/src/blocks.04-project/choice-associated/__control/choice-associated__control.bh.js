module.exports = function(bh) {
    bh.match('choice-associated__control', function(ctx, json) {
        ctx.tParam('ID', ctx.generateId(), true);
        ctx.tag('label').attrs({
            for: ctx.tParam('ID'),
        });
        ctx.content([
            {tag: 'input', attrs: {type: 'radio', name: json.name || ctx.tParam('NAME'), id: ctx.tParam('ID'), value: json.value || ctx.content()}},
            {elem: 'pill', content: [
                {tag: 'span', content: ctx.content()},
            ]},
        ], true);
    });
};
