module.exports = [
    {block: 'teaser', content: [
        {elem: 'logo', content: [
            {block: 'image', mods: {size: '38x38'}},
            'Локо-Банк',
        ]},
        {elem: 'name', content: '«Доходная стратегия с ИСЖ»'},
        {elem: 'description', content: 'до 3 000 000 руб.'},
    ]},
];
