module.exports = {
  block: 'page',
  title: 'Банк - детальная',
  content: [
    require('./common/header.bemjson'),
    {block: 'main', content: [
      {mix: {block: 'container'}, content: [
        require('./common/breadcrumb.bemjson'),
        {block: 'row', cls: 'mb-4', content: [
          {block: 'col-12', cls: 'col-lg-7 col-xl-8', content: [
            // Главный заголовок
            {block: 'title-compound', content: [
              {elem: 'header', content: 'ВТБ 24'},
            ]},
            // Блок: Тизер банка
            {block: 'mb-3 d-lg-none', content: [
              require('./common/card-bank-panel.bemjson'),
            ]},
            // Блок: Панель навигации по разделу
            {block: 'mb-3 d-lg-none', content: [
              require('../blocks.04-project/panel/panel.tmpl-specs/01-panel-menu.bemjson').new(
                  'Банки и компании',
                  ['Банки Волгограда', 'Банки', 'Микрофинансовые организации', 'Бюро кредитных историй'],
                  true
              ),
            ]},
            // Блок предложений
            {block: 'mb-3', content: [
              require('./common/bank-tabs.bemjson'),
            ]},
            // Инструменты для страницы
            require('./common/page-tools.bemjson'),
          ]},
          {block: 'col-12', cls: 'col-lg-5 col-xl-4', content: [
            // Блок: Тизер банка
            {block: 'mb-4 d-none d-lg-block', content: [
              require('./common/card-bank-panel.bemjson'),
            ]},
            // Блок: Панель навигации по разделу
            {block: 'mb-4 d-none d-lg-block', content: [
              require('../blocks.04-project/panel/panel.tmpl-specs/01-panel-menu.bemjson').new(
                  'Банки и компании',
                  ['Банки Волгограда', 'Банки', 'Микрофинансовые организации', 'Бюро кредитных историй'],
                  true
              ),
            ]},
            // Блок: Банер Вертикальный
            {block: 'sticky-top', mods: {offset: 'top'}, cls: 'h-auto mt-4 zindex-auto', content: [
              require('./common/suggestion-vertical.bemjson'),
            ]},
          ]},
        ]},
        // Блок: Банер широкий
        {block: 'row', cls: 'mb-4', content: [
          {block: 'col-12', content: [
            require('./common/suggestion-horizontal.bemjson'),
          ]},
        ]},
        // Блок: Подписка на рассылку
        {block: 'row', content: [
          {block: 'col-12', content: [
            require('./common/subscribe.bemjson'),
          ]},
        ]},
      ]},
      require('./common/sidebar.bemjson'),
    ]},
    require('./common/footer.bemjson'),
  ],
};
