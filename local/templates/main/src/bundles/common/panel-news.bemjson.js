module.exports = [
    {block: 'panel', cls: 'h-md-100 bg-light', content: [
        {elem: 'wrap', cls: 'position-md-absolute', content: [
            {elem: 'inner', content: [
                {elem: 'wrap', cls: 'position-md-absolute', content: [
                    {elem: 'body', cls: 'd-none d-md-block', content: [
                        {elem: 'shadow'},
                        {elem: 'scroll', mix: {block: 'custom-scroll-bar'}, cls: 'position-md-absolute', content: [
                            {cls: 'px-3', content: [
                                {elem: 'heading', cls: 'bg-light sticky-top d-none d-md-block px-0', content: [
                                    {elem: 'title', content: 'Сегодня'},
                                ]},
                                {block: 'font-size-md', content: [
                                    new Array(5).fill(require('./teaser-news.bemjson')),
                                ]},
                                {elem: 'heading', cls: 'bg-light sticky-top d-none d-md-block px-0', content: [
                                    {elem: 'title', content: 'Вчера'},
                                ]},
                                {block: 'font-size-md', content: [
                                    new Array(5).fill(require('./teaser-news.bemjson')),
                                ]},
                                {elem: 'heading', cls: 'bg-light sticky-top d-none d-md-block px-0', content: [
                                    {elem: 'title', content: '2 августа'},
                                ]},
                                {block: 'font-size-md', content: [
                                    new Array(5).fill(require('./teaser-news.bemjson')),
                                ]},
                                {elem: 'heading', cls: 'bg-light sticky-top d-none d-md-block px-0', content: [
                                    {elem: 'title', content: '1 августа'},
                                ]},
                                {block: 'font-size-md', content: [
                                    new Array(5).fill(require('./teaser-news.bemjson')),
                                ]},
                            ]},
                        ]},
                        {elem: 'shadow'},
                    ]},
                    {elem: 'footer', cls: 'font-size-md text-center', content: [
                        {block: 'a', content: 'Все новости'},
                    ]},
                ]},
            ]},
        ]},
    ]},
];
