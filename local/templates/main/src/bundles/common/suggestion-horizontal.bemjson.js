module.exports = [
    {block: 'suggestion', mods: {align: 'horizontal'}, cls: 'bg-light', content: [
        {elem: 'inner', content: [
            {block: 'image', mods: {size: '1000x200', aligh: 'middle'}},
        ]},
    ]},
];
