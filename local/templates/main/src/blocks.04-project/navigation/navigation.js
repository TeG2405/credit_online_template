import {Modal, Util} from 'bootstrap';
(($) => {
    const ClassName = {
        BLOCK: 'navigation',
        DROPDOWN: 'navigation__dropdown',
        SHOW: 'show',
        OPEN: 'modal-open',
        BACKDROP: 'modal-backdrop',
        FADE: 'fade',
    };
    const Event = {
        SHOW: 'show.iv.open',
        HIDE: 'hide.iv.open',
    };
    const Selector = {
        BLOCK: '.navigation',
    };
    const fakeModal = new Modal(document.createElement('div'));
    Array.from(document.getElementsByClassName(ClassName.BLOCK)).forEach((block) => {
        if (window.getComputedStyle(block).getPropertyValue('position') != 'fixed' || window.matchMedia('(min-width: 992px)').matches) {
            let poppers = Array.from(block.getElementsByClassName(ClassName.DROPDOWN)).map((dropdown) => {
                return new Popper(dropdown.parentElement, dropdown, {
                    eventsEnabled: true,
                    placement: 'bottom-start',
                    modifiers: {
                        preventOverflow: {
                            enabled: true,
                            priority: ['left', 'right'],
                            boundariesElement: block,
                        },
                    },
                });
            });
            block.addEventListener('mouseover', (event) => {
                poppers.forEach((popper) => {
                    popper.update();
                });
            });
        }
    });
    $(document)
        .on(Event.SHOW, Selector.BLOCK, (event)=>{
            if (window.getComputedStyle(event.target).getPropertyValue('position') == 'fixed') {
                fakeModal._checkScrollbar();
                fakeModal._setScrollbar();
                document.body.classList.add(ClassName.OPEN);
                if (!fakeModal._backdrop) {
                    fakeModal._backdrop = document.createElement('div');
                    fakeModal._backdrop.classList.add(ClassName.BACKDROP);
                    fakeModal._backdrop.classList.add(ClassName.FADE);
                    $(fakeModal._backdrop)
                        .one('click', () => {
                            $(event.target)
                                .removeClass(ClassName.SHOW)
                                .trigger(new $.Event(Event.HIDE));
                        })
                        .appendTo(event.target.parentElement);
                    Util.reflow(fakeModal._backdrop);
                }
                fakeModal._backdrop.classList.add(ClassName.SHOW);
            }
        })
        .on(Event.HIDE, Selector.BLOCK, (event)=>{
            if (fakeModal._backdrop) {
                document.body.classList.remove(ClassName.OPEN);
                fakeModal._resetScrollbar();
                fakeModal._backdrop.classList.remove(ClassName.SHOW);
                $(fakeModal._backdrop)
                    .one(Util.TRANSITION_END, () => {
                        fakeModal._removeBackdrop();
                    })
                    .emulateTransitionEnd(Util.getTransitionDurationFromElement(event.target));
            }
        });
})($);
