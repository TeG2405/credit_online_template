module.exports = [
    {block: 'modal', cls: 'fade', attrs: {id: 'MODAL_AGREEMENT'}, content: [
        {elem: 'dialog', cls: 'modal-dialog-centered modal-lg', content: [
            {elem: 'header', cls: 'pb-0', content: [
                {elem: 'title', content: 'Соглашение на обработку персональных данных'},
                {elem: 'close', content: {block: 'fi', mods: {icon: 'close'}}},
            ]},
            {elem: 'body', cls: 'pt-m', content: [
                {tag: 'p', content: '*Текст соглашения*'},
            ]},
            {elem: 'footer', content: [
                {block: 'btn', cls: 'btn btn-secondary', tag: 'button', attrs: {'data-dismiss': 'modal', 'onclick': `SUBSCRIBE_AGREEMENT.checked = false, SUBSCRIBE_AGREEMENT_SUBMIT.disabled = true`}, content: 'Не принимаю'},
                {block: 'btn', cls: 'btn btn-primary', tag: 'button', attrs: {'data-dismiss': 'modal', 'onclick': `SUBSCRIBE_AGREEMENT.checked = true, SUBSCRIBE_AGREEMENT_SUBMIT.disabled = false`}, content: 'Принимаю'},
            ]},
        ]},
    ]},
];
