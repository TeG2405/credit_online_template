module.exports = [
    {block: 'modal', cls: 'fade', attrs: {id: 'MODAL_AUTHORIZATION'}, content: [
        {elem: 'dialog', cls: 'modal-dialog-centered modal-sm', content: [
            {elem: 'header', cls: 'pb-0', content: [
                {elem: 'title', content: 'Авторизация'},
                {elem: 'close', content: {block: 'fi', mods: {icon: 'close'}}},
            ]},
            {elem: 'body', cls: 'pt-m', content: [
                {block: 'mb-2', content: 'Пожалуйста, авторизуйтесь'},
                {block: 'social', cls: 'mb-3', content: [
                    {elem: 'list', content: [
                        'fb',
                        'vk',
                        'ok',
                    ]},
                ]},
                {block: 'needs-validation', cls: 'font-size-md', content: [
                    {block: 'form-group', cls: 'row', content: [
                        {block: 'col-form-label', cls: 'required col-12 col-sm-3', content: 'Логин'},
                        {block: 'col-12', cls: 'col-sm-9', content: [
                            {block: 'form-control', attrs: {autocomplete: 'username', required: true}},
                            {block: 'tooltip-form-control', cls: 'invalid-feedback', content: {block: 'text-white', content: 'Поле заполнено не верно'}},
                        ]},
                    ]},
                    {block: 'form-group', cls: 'row', content: [
                        {block: 'col-form-label', cls: 'required col-12 col-sm-3', content: 'Пароль'},
                        {block: 'col-12', cls: 'col-sm-9', content: [
                            {block: 'form-control', attrs: {type: 'password', required: true}},
                            {block: 'tooltip-form-control', cls: 'invalid-feedback', content: {block: 'text-white', content: 'Поле заполнено не верно'}},
                        ]},
                    ]},
                    {block: 'form-group', cls: 'row', content: [
                        {block: 'col-12', content: [
                            {block: 'a', content: 'Забыли пароль?'},
                        ]},
                    ]},
                    {block: 'form-group', cls: 'row', content: [
                        {block: 'col-12', content: [
                            {block: 'form-check', checked: true, content: 'Запомнить меня на этом компьютере'},
                        ]},
                    ]},
                    {block: 'btn', attrs: {type: 'submit'}, tag: 'button', cls: 'btn-primary btn-block', content: 'Войти'},
                ]},
            ]},
            {elem: 'body', cls: 'font-size-md bg-blue-light', content: [
                {block: 'mb-m', content: 'Если вы впервые на сайте, заполните, пожалуйста, регистрационную форму'},
                {block: 'a', content: 'Зарегистрироваться'},
            ]},
            {elem: 'footer', content: [
                {block: 'w-100', cls: 'font-size-md text-left', content: [
                    {tag: 'span', cls: 'text-primary', content: '*'},
                    {tag: 'span', content: ' - обязательны для заполнения'},
                ]},
            ]},
        ]},
    ]},
];
