(($)=>{
    var scroll = 0;
    var $block = $('.sidebar__page-up');
    $(window).on('scroll', ()=>{
        if (scroll < window.pageYOffset || window.pageYOffset == 0) {
            $block.removeClass('show');
        } else {
            $block.addClass('show');
        }
        scroll = window.pageYOffset;
    });
})($);
