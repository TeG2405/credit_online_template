module.exports = {
  block: 'page',
  title: 'Личный кабинет - список',
  content: [
    require('./common/header.bemjson'),
    {block: 'main', content: [
      {mix: {block: 'container'}, content: [
        require('./common/breadcrumb.bemjson'),
        {block: 'row', cls: 'mb-4', content: [
          {block: 'col-12', cls: 'col-lg-7 col-xl-8', content: [
            // Главный заголовок
            {block: 'title-compound', content: [
              {elem: 'header', content: 'Личный кабинет'},
            ]},
            // Блок: Панель навигации по разделу
            {block: 'mb-3 d-lg-none', content: [
                require('../blocks.04-project/panel/panel.tmpl-specs/01-panel-menu.bemjson').new(
                    'Личный кабинет',
                    ['Регисстрация', 'Авторизация', 'Восстановление пароля', 'Профиль пользователя', 'Избранные продукты и услуги', 'Сравнение продуктов и услуг', 'Выход'],
                    true
                ),
            ]},
            // Блок навигации по разделам
            {cls: 'row vertical-gutters', content: [['Профиль пользователя', 'user'], ['Избранные продукты и услуги', 'heart-o'], ['Сравнение продуктов и услуг', 'layers']].map((item) => [
              {cls: 'col-12 col-md-6 col-lg-12 col-xl-6', content: [
                {block: 'card-section', content: [
                  {elem: 'icon', content: [
                    {block: 'fi', mods: {icon: item[1]}},
                  ]},
                  {elem: 'title', content: item[0]},
                ]},
              ]},
            ])},
          ]},
          {block: 'col-12', cls: 'col-lg-5 col-xl-4', content: [
            // Блок: Панель навигации по разделу
            {block: 'mb-4 d-none d-lg-block', content: [
              require('../blocks.04-project/panel/panel.tmpl-specs/01-panel-menu.bemjson').new(
                  'Личный кабинет',
                  ['Регисстрация', 'Авторизация', 'Восстановление пароля', 'Профиль пользователя', 'Избранные продукты и услуги', 'Сравнение продуктов и услуг', 'Выход'],
                  false
              ),
            ]},
            // Блок: Банер Вертикальный
            {block: 'sticky-top', mods: {offset: 'top'}, cls: 'h-auto mt-4 zindex-auto', content: [
              require('./common/suggestion-vertical.bemjson'),
            ]},
          ]},
        ]},
        // Блок: Банер широкий
        {block: 'row', cls: 'mb-4', content: [
          {block: 'col-12', content: [
            require('./common/suggestion-horizontal.bemjson'),
          ]},
        ]},
        // Блок: Подписка на рассылку
        {block: 'row', content: [
          {block: 'col-12', content: [
            require('./common/subscribe.bemjson'),
          ]},
        ]},
      ]},
      require('./common/sidebar.bemjson'),
    ]},
    require('./common/footer.bemjson'),
  ],
};
