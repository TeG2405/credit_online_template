module.exports = [
    {block: 'page-tools', cls: 'my-3', content: [
        {cls: 'row', content: [
            {cls: 'col-12 col-sm-4 text-sm-right order-sm-1', content: [
                {elem: 'controls', content: [
                    {elem: 'icon', cls: 'a', tag: 'a', attrs: {href: '#'}, content: [
                        {block: 'fi', mods: {icon: 'heart-o'}, cls: 'align-middle'},
                    ]},
                    {elem: 'icon', cls: 'a', tag: 'a', attrs: {href: '#'}, content: [
                        {block: 'fi', mods: {icon: 'layers'}, cls: 'align-middle'},
                    ]},
                    {elem: 'icon', content: [
                        {block: 'fi', mods: {icon: 'visualization'}, cls: 'align-middle'},
                        {tag: 'small', cls: 'align-middle ml-x', content: '100'},
                    ]},
                ]},
            ]},
            {cls: 'col-12 col-sm-8 mt-2 mt-sm-0', content: [
                {block: 'share', content: [
                    {elem: 'title', cls: 'd-none d-sm-block', content: 'Поделиться'},
                    {elem: 'inner', content: [
                        {block: 'social-likes', content: ['facebook', 'twitter', 'plusone', 'mailru', 'pinterest', 'vkontakte', 'odnoklassniki']},
                    ]},
                ]},
            ]},
        ]},
    ]},
];
