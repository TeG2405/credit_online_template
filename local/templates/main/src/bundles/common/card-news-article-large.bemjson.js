module.exports = [
    {block: 'card-news', content: [
        {elem: 'image', content: [
            {block: 'image', mods: {size: '690x420'}},
        ]},
        {elem: 'body', content: [
            {elem: 'caption', content: [
                {elem: 'title', cls: 'font-size-xl-h5', content: 'Четверть кредитных карт россиян остается «мертвыми»'},
                {elem: 'description', content: [
                    {tag: 'p', content: 'Совет директоров Банка России на первом заседании в текущем году принял решение понизить ключевую ставку на 25 базисных пунктов — до 7,5% годовых.'},
                    {tag: 'p', content: 'Совет директоров Банка России на первом заседании в текущем году принял решение понизить ключевую ставку на 25 базисных пунктов — до 7,5% годовых.'},
                ]},
            ]},
        ]},
    ]},
];
