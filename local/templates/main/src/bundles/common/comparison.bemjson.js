module.exports = [
    {block: 'comparison', content: [
        {elem: 'head', content: [
            {elem: 'row', cls: 'row', content: [
                {elem: 'title', cls: 'col-12 col-md-3 col-lg-12 col-xl-3'},
                {elem: 'col', cls: 'col-12 col-md-9 col-lg-12 col-xl-9', content: [
                    {cls: 'row', content: [
                        {cls: 'w-100', content: [
                            {cls: 'swiper-container swiper-container-horizontal', content: [
                                {elem: 'button', mods: {prev: true}, content: [
                                    {block: 'a', mix: {block: 'fi', mods: {icon: 'angle-left'}}},
                                ]},
                                {cls: 'swiper-wrapper', content: new Array(20).fill([
                                    {cls: 'col-6 col-sm-4 swiper-slide text-center', content: [
                                        {block: 'a', cls: 'd-block', content: [
                                            {block: 'image', mods: {size: '160x80'}, content: [
                                                {block: 'img', width: 160, height: 80, mods: {lazy: true}, src: 'http://placehold.it/160x80'},
                                            ]},
                                            {cls: 'my-m', content: [
                                                {block: 'rating'},
                                            ]},
                                            {block: 'mt-m', content: 'Банк Тинькофф – Кредиты наличными'},
                                        ]},
                                        {block: 'btn', cls: 'btn-primary btn-sm btn-block mt-m', content: 'Оформить'},
                                    ]},
                                ])},
                                {elem: 'button', mods: {next: true}, content: [
                                    {block: 'a', mix: {block: 'fi', mods: {icon: 'angle-right'}}},
                                ]},
                            ]},
                        ]},
                    ]},
                ]},
            ]},
        ]},
        {elem: 'body', content: new Array(20).fill([
            {elem: 'row', content: [
                {elem: 'title', cls: 'col-12 col-md-3 col-lg-12 col-xl-3', content: 'Процентная ставка'},
                {elem: 'col', cls: 'col-12 col-md-9 col-lg-12 col-xl-9', content: [
                    {cls: 'row', content: [
                        {cls: 'w-100', content: [
                            {cls: 'swiper-container swiper-container-horizontal', content: [
                                {cls: 'swiper-wrapper', content: new Array(10).fill([
                                    {cls: 'col-6 col-sm-4 swiper-slide', content: 'от 11.9%'},
                                ])},
                            ]},
                        ]},
                    ]},
                ]},
            ]},
        ])},
    ]},
];
