const ITEMS = [
    ['Кредит наличными',
        'hand-money.svg',
        '<b>3500</b> предложений', 'large'],
    ['Микрозайм',
        'hand-coins.svg',
        '<b>3500</b> предложений', ''],
    ['Кредит для бизнеса',
        'briefcase.svg',
        '<b>3500</b> предложений', ''],
    ['Кредитные карты',
        'credit-card.svg',
        '<b>3500</b> предложений', 'middle'],
    ['Автокредит',
        'electric-car.svg',
        '<b>3500</b> предложений', 'middle'],
    ['Курс валют',
        'coins.svg',
        '<b>3500</b> предложений', ''],
    ['Дебетовые карты',
        'debit-card.svg',
        '<b>3500</b> предложений', ''],
    ['Заявка в банки',
        'bank.svg',
        '<b>Одна заявка</b> во все банки', ''],
    ['Заявка в МФО',
        'message.svg',
        '<b>3500</b> предложений', ''],
    ['Ипотека',
        'mortgage.svg',
        '<b>3500</b> предложений', 'middle'],
    ['Вклады',
        'piggy-bank.svg',
        '<b>3500</b> предложений', 'middle'],
    ['Кредитный калькулятор',
        'calculator.svg',
        '<b>3500</b> предложений', 'middle'],
];
module.exports = [
    {block: 'tile', cls: 'row no-gutters', content: ITEMS.map((item) => {
        switch (item[3]) {
            case 'large':
                return [
                    {elem: 'item', cls: 'col-12 col-sm-6 col-md-4 col-xl-12', content: [
                        {cls: 'row no-gutters w-100 align-items-center py-xl-3', content: [
                            {cls: 'col-12 col-xl-3', content: [
                                {elem: 'icon', content: [
                                    {block: 'image', mods: {size: '120x90', align: 'middle'}, content: [
                                        {block: 'img', mods: {lazy: true}, src: 'upload/tile/'+item[1]},
                                        {elem: 'char', cls: 'text-primary', content: item[0][0]},
                                    ]},
                                    {elem: 'badge', content: [
                                        {tag: 'span', content: item[2]},
                                    ]},
                                ]},
                            ]},
                            {cls: 'col-12 col-xl-9 text-xl-left pr-xl-3', content: [
                                {elem: 'title', cls: 'mt-xl-0 mb-xl-m', content: item[0]},
                                {block: 'd-none', cls: 'd-xl-block', content: [
                                    {tag: 'p', content: 'Оформите кредит наличными c моментальным решением. Заполните онлайн заявку и получите деньги в ближайшем офисе банка! Для 100% одобрения отправьте заявку в несколько банков*'},
                                ]},
                            ]},
                        ]},
                    ]},
                ];
            case 'middle':
                return [
                    {elem: 'item', cls: 'col-12 col-sm-6 col-md-4 col-xl-6', content: [
                        {cls: 'row no-gutters w-100 align-items-center py-xl-2', content: [
                            {cls: 'col-12 col-xl-6', content: [
                                {elem: 'icon', content: [
                                    {block: 'image', mods: {size: '120x90', align: 'middle'}, content: [
                                        {block: 'img', mods: {lazy: true}, src: 'upload/tile/'+item[1]},
                                        {elem: 'char', cls: 'text-primary', content: item[0][0]},
                                    ]},
                                    {elem: 'badge', content: [
                                        {tag: 'span', content: item[2]},
                                    ]},
                                ]},
                            ]},
                            {cls: 'col-12 col-xl-6 text-xl-left', content: [
                                {elem: 'title', cls: 'mt-xl-0', content: item[0]},
                            ]},
                        ]},
                    ]},
                ];
            default:
                return [
                    {elem: 'item', cls: 'col-12 col-sm-6 col-md-4 col-xl-3', content: [
                        {cls: 'row no-gutters w-100 align-items-center', content: [
                            {cls: 'col-12', content: [
                                {elem: 'icon', content: [
                                    {block: 'image', mods: {size: '120x90', align: 'middle'}, content: [
                                        {block: 'img', mods: {lazy: true}, src: 'upload/tile/'+item[1]},
                                        {elem: 'char', cls: 'text-primary', content: item[0][0]},
                                    ]},
                                    {elem: 'badge', content: [
                                        {tag: 'span', content: item[2]},
                                    ]},
                                ]},
                            ]},
                            {cls: 'col-12', content: [
                                {elem: 'title', content: item[0]},
                            ]},
                        ]},
                    ]},
                ];
        }
    })},
];
