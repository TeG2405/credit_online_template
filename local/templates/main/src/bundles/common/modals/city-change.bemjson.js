const REGIONS = [
    'Центральный',
    'Северо-Западный',
    'Южный',
    'Северо-Кавказский',
    'Приволжский',
    'Уральский',
    'Сибирский',
    'Дальневосточный',
];
module.exports = [
    {block: 'modal', cls: 'fade', attrs: {id: 'MODAL_CITY_CHANGE'}, content: [
        {elem: 'dialog', cls: 'modal-dialog-centered modal-lg', content: [
            {elem: 'header', cls: 'pb-0', content: [
                {elem: 'title', content: 'Выбор города'},
                {elem: 'close', content: {block: 'fi', mods: {icon: 'close'}}},
            ]},
            {elem: 'body', content: [
                {block: 'input-group', content: [
                    {block: 'form-control', cls: 'border-right-0', attrs: {id: 'MODAL_CITY_CHANGE_INPUT', placeholder: 'Введите названия города'}},
                    {elem: 'append', cls: 'input-group-append', content: [
                        {block: 'btn', cls: 'btn-outline-blue bg-transparent border-left-0 text-primary', content: {
                            block: 'fi', mods: {icon: 'search'},
                        }},
                    ]},
                ]},
                {block: 'mt-s', cls: 'font-size-small text-muted', content: [
                    'Например:',
                    {block: 'a', mix: {block: 'choice-associated', elem: 'pill'}, cls: 'ml-2', attrs: {onclick: `document.getElementById('MODAL_CITY_CHANGE_INPUT').value = this.textContent`}, content: [
                        {tag: 'span', content: 'Челябинск'},
                    ]},
                    {block: 'a', mix: {block: 'choice-associated', elem: 'pill'}, cls: 'ml-2', attrs: {onclick: `document.getElementById('MODAL_CITY_CHANGE_INPUT').value = this.textContent`}, content: [
                        {tag: 'span', content: 'Москва'},
                    ]},
                ]},
            ]},
            {elem: 'body', cls: 'bg-blue-light', content: [
                {block: 'choice-associated', content: [
                    {cls: 'row', content: [
                        {elem: 'col', cls: 'col-12 col-sm-4', content: [
                            {elem: 'title', content: 'Округ'},
                            {elem: 'scroll', content: [
                                {mix: {block: 'custom-scroll-bar'}, content: [
                                    {elem: 'list', attrs: {'data-target': '#MODAL_CITY_CHANGE_REGION', 'action': 'stubs/regions.json'}, content: REGIONS.map((item)=> [
                                        {elem: 'item', content: [
                                            {elem: 'control', content: item},
                                        ]},
                                    ])},
                                ]},
                            ]},
                        ]},
                        {elem: 'col', cls: 'col-12 col-sm-5', content: [
                            {elem: 'title', content: 'Область, республика, край'},
                            {elem: 'scroll', content: [
                                {mix: {block: 'custom-scroll-bar'}, content: [
                                    {elem: 'list', attrs: {'id': 'MODAL_CITY_CHANGE_REGION', 'data-target': '#MODAL_CITY_CHANGE_CITY', 'action': 'stubs/city.json'}},
                                ]},
                            ]},
                        ]},
                        {elem: 'col', cls: 'col-12 col-sm-3', content: [
                            {elem: 'title', content: 'Город'},
                            {elem: 'scroll', content: [
                                {mix: {block: 'custom-scroll-bar'}, content: [
                                    {elem: 'list', attrs: {'id': 'MODAL_CITY_CHANGE_CITY'}},
                                ]},
                            ]},
                        ]},
                    ]},
                ]},
            ]},
        ]},
    ]},
];
