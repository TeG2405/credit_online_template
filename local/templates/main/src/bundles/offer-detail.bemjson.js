module.exports = {
  block: 'page',
  title: 'Предложение - детальная',
  content: [
    require('./common/header.bemjson'),
    {block: 'main', content: [
      {mix: {block: 'container'}, content: [
        require('./common/breadcrumb.bemjson'),
        {block: 'row', cls: 'mb-4', content: [
          {block: 'col-12', cls: 'col-lg-7 col-xl-8', content: [
            // Главный заголовок
            {block: 'title-compound', content: [
              {elem: 'header', content: 'Банк Тинькофф – Кредитные карты'},
            ]},
            // Подробное описание предложения
            require('./common/card-offer-light.bemjson'),
            // Блок: Тизер банка
            {block: 'mb-3 d-lg-none', content: [
              require('./common/card-bank-panel.bemjson'),
            ]},
            // Блок: Панель навигации по разделу
            {block: 'mb-3 d-lg-none', content: [
              require('../blocks.04-project/panel/panel.tmpl-specs/01-panel-menu.bemjson').new(
                  'Продукты и услуги',
                  [['Кредит наличными', 'hand-money.svg'], ['Микрозайм', 'hand-coins.svg'], ['Кредит для бизнеса', 'briefcase.svg'], ['Кредитные карты', 'credit-card.svg'], ['Автокредит', 'electric-car.svg'], ['Курс валют', 'coins.svg'], ['Дебетовые карты', 'debit-card.svg'], ['Заявка в банки', 'bank.svg'], ['Заявка в МФО', 'message.svg'], ['Ипотека', 'mortgage.svg'], ['Вклады', 'piggy-bank.svg'], ['Кредитный калькулятор', 'calculator.svg']],
                  true
              ),
            ]},
            // Описание прадложения в табах
            require('./common/offer-tabs.bemjson'),
            // Инструменты для страницы
            require('./common/page-tools.bemjson'),
            // Блок: Карусель предложений
            {block: 'h', size: 4, cls: 'font-weight-normal', content: 'Похожие предложения'},
            require('./common/swiper-row-offer.bemjson'),
            // Отзывы
            {block: 'h', size: 4, cls: 'font-weight-normal', content: 'Последние отзывы'},
            {content: [
              new Array(3).fill(require('./common/review.bemjson')),
              {block: 'a', content: 'Все отзывы'},
            ]},
          ]},
          {block: 'col-12', cls: 'col-lg-5 col-xl-4', content: [
            // Блок: Тизер банка
            {block: 'mb-4 d-none d-lg-block', content: [
              require('./common/card-bank-panel.bemjson'),
            ]},
            // Блок: Панель навигации по разделу
            {block: 'mb-4 d-none d-lg-block', content: [
              require('../blocks.04-project/panel/panel.tmpl-specs/01-panel-menu.bemjson').new(
                  'Продукты и услуги',
                  [['Кредит наличными', 'hand-money.svg'], ['Микрозайм', 'hand-coins.svg'], ['Кредит для бизнеса', 'briefcase.svg'], ['Кредитные карты', 'credit-card.svg'], ['Автокредит', 'electric-car.svg'], ['Курс валют', 'coins.svg'], ['Дебетовые карты', 'debit-card.svg'], ['Заявка в банки', 'bank.svg'], ['Заявка в МФО', 'message.svg'], ['Ипотека', 'mortgage.svg'], ['Вклады', 'piggy-bank.svg'], ['Кредитный калькулятор', 'calculator.svg']],
                  true
              ),
            ]},
            // Блок: Банер Вертикальный
            {block: 'sticky-top', mods: {offset: 'top'}, cls: 'h-auto mt-4 zindex-auto', content: [
              require('./common/suggestion-vertical.bemjson'),
            ]},
          ]},
        ]},
        // Блок: Банер широкий
        {block: 'row', cls: 'mb-4', content: [
          {block: 'col-12', content: [
            require('./common/suggestion-horizontal.bemjson'),
          ]},
        ]},
        // Блок: Подписка на рассылку
        {block: 'row', content: [
          {block: 'col-12', content: [
            require('./common/subscribe.bemjson'),
          ]},
        ]},
      ]},
      require('./common/sidebar.bemjson'),
    ]},
    require('./common/footer.bemjson'),
  ],
};
