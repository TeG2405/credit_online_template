module.exports = function(bh) {
    bh.match('navigation__close', function(ctx, json) {
        ctx.tag('a').attrs({
            'href': '#NAVIGATION',
            'data-toggle': 'open',
        });
    });
};
