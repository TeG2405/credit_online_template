import Swiper from 'swiper/dist/js/swiper.min';
const ClassName = {
    BLOCK: 'comparison',
    SLIDER: 'swiper-container',
    BODY: 'comparison__body',
};
(() => {
    Array.from(document.getElementsByClassName(ClassName.BLOCK)).forEach((block)=>{
        let next = block.getElementsByClassName('comparison__button_next');
        let prev = block.getElementsByClassName('comparison__button_prev');
        let instance = Array.from(block.getElementsByClassName(ClassName.SLIDER)).map((slider, index) => {
            return new Swiper(slider, {
                slidesPerView: 'auto',
                spaceBetween: 0,
                navigation: {
                    nextEl: next,
                    prevEl: prev,
                },
                on: {
                    'transitionStart': ()=>{
                        instance.slice(1).forEach((slider)=>{
                            slider.$el
                                .addClass('fade')
                                .removeClass('show');
                        });
                    },
                    'transitionEnd': (data)=>{
                        instance.slice(1).forEach((slider)=>{
                            slider.$el.addClass('show');
                        });
                    },
                },
            });
        });
    });
})();
