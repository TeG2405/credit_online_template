const ITEMS = [
    'Народный рейтинг',
    'Финансовые показатели',
    'Кредитные рейтинги',
];
const ALL = [
    'народные рейтинги',
    'финансовые показатели',
    'кредитные рейтинги',
];
module.exports = [
    {block: 'nav', mods: {type: 'simple'}, cls: 'mb-2', content: [
        ITEMS.map((item, index)=>[
            {elem: 'item', cls: 'nav-item', content: [
                {block: 'a', cls: !index ? 'nav-link active' : 'nav-link', attrs: {'data-toggle': 'tab', 'href': '#TABLE_RATING_TAB_'+index}, content: [
                    {tag: 'span', content: item},
                ]},
            ]},
        ]),
    ]},
    {cls: 'tab-content', content: [
        ITEMS.map((item, index)=>[
            {mix: {block: 'tab-pane'}, cls: index ? '' : 'active', attrs: {id: 'TABLE_RATING_TAB_'+index}, content: [
                {block: 'bg-light', cls: 'py-2', content: [
                    {block: 'table', cls: 'table-sm-block table-borderless table-hover mb-0', tag: 'table', content: [
                        {tag: 'thead', content: [
                            {tag: 'tr', content: [
                                {tag: 'th', cls: 'text-center', content: 'Позиция'},
                                {tag: 'th', content: 'Банк или компания'},
                                {tag: 'th', cls: 'text-center', content: 'Рейтинг'},
                                {tag: 'th', cls: 'text-center', content: 'Количество отзывов'},
                            ]},
                        ]},
                        {tag: 'tbody', content: new Array(Math.floor(Math.random() * 5) + 5).fill('').map((item, index)=>[
                            {tag: 'tr', content: [
                                {tag: 'td', cls: 'text-md-center order-1', attrs: {'data-title': 'Позиция'}, content: ++index},
                                {tag: 'td', content: [
                                    {block: 'a', content: 'Тинькофф Банк'},
                                ]},
                                {tag: 'td', cls: 'text-md-center order-1', attrs: {'data-title': 'Рейтинг'}, content: Math.floor(Math.random() * 5)},
                                {tag: 'td', cls: 'text-md-center order-1', attrs: {'data-title': 'Количество отзывов'}, content: [
                                    {block: 'a', content: Math.floor(Math.random() * 500)},
                                ]},
                            ]},
                        ])},
                    ]},
                ]},
                {block: 'mt-2', content: [
                    {block: 'a', content: 'Все ' + ALL[index]},
                ]},
            ]},
        ]),
    ]},
];
