const ITEMS = [
    ['Чекбоксы', 'checkbox'],
    ['Радиокнопки', 'radio'],
    ['Диапазон', 'range'],
    ['Текстовой поле', 'input'],
    ['Текстовой поле диапазон', 'input-range'],
    ['Селект', 'select'],
    ['Селект множественный', 'multiple'],
];
const VALUES = [
    'Кредит на 50000 рублей',
    'Кредит на 100000 рублей',
    'Кредит на 200000 рублей',
    'Кредит на 300000 рублей',
    'Кредит на 1000000 рублей',
    'Кредит на 2000000 рублей',
    'Кредит на 3000000 рублей',
    'Кредит на 300000 рублей',
    'Кредит на 1000000 рублей',
    'Кредит на 2000000 рублей',
    'Кредит на 3000000 рублей',
];
const getView = function(view) {
    switch (view) {
        case 'checkbox':
            return [
                {elem: 'row', content: [
                    {elem: 'col', content: VALUES.slice(0, VALUES.length > 10 ? Math.ceil(VALUES.length / 2) : 10).map((item)=>[
                        {block: 'form-check', content: item},
                    ])},
                    {elem: 'col', content: VALUES.slice(VALUES.length > 10 ? Math.ceil(VALUES.length / 2) + 1 : 10).map((item)=>[
                        {block: 'form-check', content: item},
                    ])},
                ]},
            ];
        case 'radio':
            var rand = Math.floor(Math.random() * 1000);
            return [
                {elem: 'row', content: [
                    {elem: 'col', content: VALUES.slice(0, VALUES.length > 10 ? Math.ceil(VALUES.length / 2) : 10).map((item)=>[
                        {block: 'form-check', type: 'radio', name: 'RADIO'+rand, content: item},
                    ])},
                    {elem: 'col', content: VALUES.slice(VALUES.length > 10 ? Math.ceil(VALUES.length / 2) + 1 : 10).map((item)=>[
                        {block: 'form-check', type: 'radio', name: 'RADIO'+rand, content: item},
                    ])},
                ]},
            ];
        case 'input':
            return [
                {block: 'form-control', placeholder: '0', content: ''},
            ];
        case 'input-range':
            return [
                {mix: {block: 'range'}, content: [
                    {cls: 'd-flex row', content: [
                        {cls: 'pr-1 col-6', content: [
                            {mix: {block: 'range', elem: 'control'}, cls: 'form-control', content: [
                                'От:',
                                {mix: {block: 'range', elem: 'input'}, tag: 'input', attrs: {placeholder: 0}, content: 0},
                            ]},
                        ]},
                        {cls: 'pl-1 col-6', content: [
                            {mix: {block: 'range', elem: 'control'}, cls: 'form-control', content: [
                                'До:',
                                {mix: {block: 'range', elem: 'input'}, tag: 'input', attrs: {placeholder: 1500}, content: 1500},
                            ]},
                        ]},
                    ]},
                ]},
            ];
        case 'select':
            return [
                {block: 'form-control', tag: 'select', content: VALUES},
            ];
        case 'multiple':
            return [
                {block: 'form-control', tag: 'select', attrs: {multiple: true}, content: VALUES},
            ];
        case 'range':
            return [
                {block: 'range', range: [0, 1500]},
            ];
    }
};
module.exports = [
    {block: 'navigation', mix: {block: 'filter'}, tag: 'form', attrs: {'id': 'FILTER', 'action': '/', 'data-target': '#FILTER_LIST'}, content: [
        {block: 'btn', cls: 'btn-primary btn-sm d-none', mix: {block: 'filter', elem: 'submit'}, tag: 'button', attrs: {type: 'submit'}, content: 'Показать (100500)'},
        {elem: 'close', attrs: {'href': '#FILTER'}, content: {block: 'fi', mods: {icon: 'close'}}},
        {elem: 'inner', content: [
            {elem: 'list', cls: 'justify-content-start', content: [
                ITEMS.map((item)=>[
                    {elem: 'item', content: [
                        {elem: 'link', content: [
                            item[0],
                            {elem: 'icon', mix: {block: 'fi', mods: {icon: 'angle-down'}}},
                        ]},
                        {elem: 'dropdown', mix: {block: 'filter', elem: 'dropdown'}, content: [
                            getView(item[1]),
                            {mix: {block: 'filter', elem: 'control'}},
                        ]},
                    ]},
                ]),
                {cls: 'w-100'},
                {elem: 'item', content: [
                    {elem: 'link', tag: 'a', attrs: {href: '#'}, cls: 'text-primary', content: [
                        'Показать все фильтры',
                    ]},
                ]},
                {elem: 'item', content: [
                    {elem: 'link', tag: 'a', attrs: {href: '#'}, cls: 'text-primary', content: [
                        'Очистить фильтр',
                    ]},
                ]},
            ]},
        ]},
    ]},
];
