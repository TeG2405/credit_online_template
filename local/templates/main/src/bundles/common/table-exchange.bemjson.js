module.exports = [
    {block: 'table-exchange', tag: 'table', content: [
        {tag: 'tbody', content: [
            {tag: 'tr', content: [
                {tag: 'td', content: 'USD'},
                {tag: 'th', content: [
                    'Доллар США',
                ]},
                {tag: 'td', content: [
                    '61,12',
                ]},
            ]},
            {tag: 'tr', content: [
                {tag: 'td', content: 'EUR'},
                {tag: 'th', content: [
                    'Евро',
                ]},
                {tag: 'td', content: [
                    '61,12',
                ]},
            ]},
        ]},
    ]},
];
