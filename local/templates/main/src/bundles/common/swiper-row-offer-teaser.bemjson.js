const ITEMS = [
    ['Кредит наличными', 'hand-money.svg', '<b>3500</b> предложений'],
    ['Микрозайм', 'hand-coins.svg', '<b>3500</b> предложений'],
    ['Кредит для бизнеса', 'briefcase.svg', '<b>3500</b> предложений'],
    ['Кредитные карты', 'credit-card.svg', '<b>3500</b> предложений'],
    ['Автокредит', 'electric-car.svg', '<b>3500</b> предложений'],
    ['Курс валют', 'coins.svg', '<b>3500</b> предложений'],
    ['Дебетовые карты', 'debit-card.svg', '<b>3500</b> предложений'],
    ['Заявка в банки', 'bank.svg', '<b>3500</b> предложений'],
    ['Заявка в МФО', 'message.svg', '<b>3500</b> предложений'],
    ['Ипотека', 'mortgage.svg', '<b>3500</b> предложений'],
    ['Вклады', 'piggy-bank.svg', '<b>3500</b> предложений'],
];
module.exports = [
    {block: 'swiper-row', cls: 'row', content: [
        {cls: 'w-100', content: [
            {cls: 'swiper-container swiper-container-horizontal', content: [
                {cls: 'swiper-wrapper', content: ITEMS.map((item) => [
                    {block: 'col-6', cls: 'col-sm-4 col-xl-3 swiper-slide', content: [
                        {block: 'text-center', content: [
                            {mix: {block: 'tile', elem: 'icon'}, content: [
                                {block: 'image', mods: {size: '80x80', align: 'middle'}, content: [
                                    {block: 'img', mods: {lazy: true}, width: 80, height: 80, src: 'upload/tile/'+item[1]},
                                    {elem: 'char', cls: 'text-primary', content: item[0][0]},
                                ]},
                                {mix: {block: 'tile', elem: 'badge'}, content: [
                                    {tag: 'span', content: item[2]},
                                ]},
                            ]},
                            {mix: {block: 'tile', elem: 'title'}, content: item[0]},
                        ]},
                    ]},
                ])},
                {cls: 'swiper-pagination position-relative mt-3 d-none d-md-block'},
            ]},
        ]},
    ]},
];