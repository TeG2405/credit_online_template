module.exports = [
    {block: 'dropdown', cls: 'mr-sm-3 w-100', content: [
        {block: 'btn', cls: 'btn-form-control btn-block dropdown-toggle-custom d-flex justify-content-between align-items-center', attrs: {'data-toggle': 'dropdown'}, content: [
            {tag: 'span', content: 'Рейтинг - по возрастанию'},
        ]},
        {block: 'dropdown-menu', cls: 'font-size-small', content: [
            'Рейтинг - по возрастанию',
            'Рейтинг - по убыванию',
        ]},
    ]},
];