module.exports = [
    {block: 'row', cls: 'mb-3', content: [
        {block: 'col-12', cls: 'col-sm-8 pr-sm-0 mb-2 mb-sm-0', content: [
            {block: 'form-control', cls: 'form-control-lg', attrs: {'data-toggle': 'autocomplite', 'data-url': 'stubs/banks.json', 'data-callback': 'stubFrontCallBack'}, placeholder: 'Введите название банка'},
        ]},
        {block: 'col-12', cls: 'col-sm-4', content: [
            {block: 'btn', cls: 'btn-primary btn-block btn-lg', content: 'Найти'},
        ]},
    ]},
];
