module.exports = {
  block: 'page',
  title: 'Личный кабинет - сравнение продуктов',
  content: [
    require('./common/header.bemjson'),
    {block: 'main', content: [
      {mix: {block: 'container'}, content: [
        require('./common/breadcrumb.bemjson'),
        {block: 'row', cls: 'mb-4', content: [
          {block: 'col-12', cls: 'col-lg-7 col-xl-8', content: [
            // Главный заголовок
            {block: 'title-compound', content: [
              {elem: 'header', content: 'Сравнение продуктов и услуг'},
            ]},
            // Блок: Панель навигации по разделу
            {block: 'mb-3 d-lg-none', content: [
                require('../blocks.04-project/panel/panel.tmpl-specs/01-panel-menu.bemjson').new(
                    'Личный кабинет',
                    ['Регисстрация', 'Авторизация', 'Восстановление пароля', 'Профиль пользователя', 'Избранные продукты и услуги', 'Сравнение продуктов и услуг', 'Выход'],
                    true
                ),
            ]},
            {block: 'mb-3', cls: 'row', content: [
              {cls: 'col-12', content: [
                {block: 'row', content: [
                  {block: 'col-12', cls: 'col-sm-6 col-xl-4', content: [
                    require('./common/dropdown-sort.bemjson'),
                  ]},
                  {block: 'col-12', cls: 'col-sm-6 col-xl-8', content: [
                    {block: 'nav', tag: 'nav', mods: {type: 'toggle'}, cls: 'mt-2 mt-sm-0', content: [
                      {block: 'a', attrs: {'data-toggle': 'tab', 'href': '#FEATURE_COMPARISON'}, cls: 'nav-item btn active show btn-blue w-100 w-sm-auto', content: [
                        {block: 'fi', cls: 'align-middle mr-1', mods: {icon: 'align-left'}},
                        {tag: 'span', cls: 'align-middle', content: 'Различающиеся парметры'},
                      ]},
                      {block: 'a', attrs: {'data-toggle': 'tab', 'href': '#FEATURE_ALL'}, cls: 'nav-item btn btn-blue w-100 w-sm-auto', content: [
                        {block: 'fi', cls: 'align-middle mr-1', mods: {icon: 'align-left'}},
                        {tag: 'span', cls: 'align-middle', content: 'Все парметры'},
                      ]},
                    ]},
                  ]},
                ]},
              ]},
            ]},
            require('./common/comparison.bemjson'),
          ]},
          {block: 'col-12', cls: 'col-lg-5 col-xl-4', content: [
            // Блок: Панель навигации по разделу
            {block: 'mb-4 d-none d-lg-block', content: [
              require('../blocks.04-project/panel/panel.tmpl-specs/01-panel-menu.bemjson').new(
                  'Личный кабинет',
                  ['Регисстрация', 'Авторизация', 'Восстановление пароля', 'Профиль пользователя', 'Избранные продукты и услуги', 'Сравнение продуктов и услуг', 'Выход'],
                  false
              ),
            ]},
            // Блок: Банер Вертикальный
            {block: 'sticky-top', mods: {offset: 'top'}, cls: 'h-auto mt-4 zindex-auto', content: [
              require('./common/suggestion-vertical.bemjson'),
            ]},
          ]},
        ]},
        // Блок: Банер широкий
        {block: 'row', cls: 'mb-4', content: [
          {block: 'col-12', content: [
            require('./common/suggestion-horizontal.bemjson'),
          ]},
        ]},
        // Блок: Подписка на рассылку
        {block: 'row', content: [
          {block: 'col-12', content: [
            require('./common/subscribe.bemjson'),
          ]},
        ]},
      ]},
      require('./common/sidebar.bemjson'),
    ]},
    require('./common/footer.bemjson'),
  ],
};
