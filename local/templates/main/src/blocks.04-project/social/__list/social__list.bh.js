module.exports = function(bh) {
    bh.match('social__list', function(ctx, json) {
        ctx.tag('ul').content([
            ctx.content().map((item)=>{
                return ctx.isSimple(item) ? {elem: 'li', content: [
                    {elem: 'link', content: [
                        {block: 'fi', mods: {icon: item}},
                    ]},
                ]} : item;
            }),
        ], true);
    });
};
