module.exports = [
    {block: 'form', content: [
        {block: 'subscribe', content: [
            {cls: 'row align-items-center', content: [
                {cls: 'col-12 col-xl-3 d-none d-xl-block', content: [
                    {elem: 'icon', content: [
                        {block: 'image', mods: {size: '240x140'}, content: [
                            {block: 'img', mods: {lazy: true}, attrs: {height: 140}, src: 'images/envelope.svg'},
                        ]},
                    ]},
                ]},
                {cls: 'col-12 col-xl-9', content: [
                    {cls: 'row align-items-center', content: [
                        {cls: 'col-12 col-lg-6', content: [
                            {elem: 'title', cls: 'h2 mt-0 mb-lg-0', content: 'Подпишитесь на нашу рассылку, чтобы быть в курсе новостей!'},
                        ]},
                        {cls: 'col-12 col-lg-6', content: [
                            {elem: 'inner', content: [
                                {cls: 'input-group input-group-destroy-down-sm input-group-lg mb-2', content: [
                                    {block: 'form-control', cls: 'form-control-lg', attrs: {type: 'email'}, placeholder: 'Введите ваш e-mail'},
                                    {cls: 'input-group-append', content: [
                                        {block: 'btn', cls: 'btn-lg btn-white btn-block px-3', tag: 'button', attrs: {type: 'submit', disabled: true, id: 'SUBSCRIBE_AGREEMENT_SUBMIT'}, content: 'Подписаться'},
                                    ]},
                                ]},
                                {block: 'form-check', cls: 'font-size-small', content: [
                                    {elem: 'input', attrs: {'data-toggle': 'agreement', 'data-target': '#MODAL_AGREEMENT', 'id': 'SUBSCRIBE_AGREEMENT'}},
                                    {elem: 'label', attrs: {for: 'SUBSCRIBE_AGREEMENT'}, content: 'Нажимая на кнопку «Подписаться», я даю свое согласие на обработку моих персональных данных'},
                                ]},
                            ]},
                        ]},
                    ]},
                ]},
            ]},
        ]},
    ]},
];

