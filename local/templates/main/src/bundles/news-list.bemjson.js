module.exports = {
  block: 'page',
  title: 'Новости - список',
  content: [
    require('./common/header.bemjson'),
    {block: 'main', content: [
      {mix: {block: 'container'}, content: [
        require('./common/breadcrumb.bemjson'),
        {block: 'row', cls: 'mb-4', content: [
          {block: 'col-12', cls: 'col-lg-7 col-xl-8', content: [
            // Главный заголовок
            {block: 'title-compound', content: [
              {elem: 'header', content: 'Новоси'},
              {elem: 'addition', cls: 'w-100 w-sm-auto mt-sm-0 align-self-center', content: [
                require('./common/dropdown-sort.bemjson'),
              ]},
            ]},
            // Блок: Панель навигации по разделу
            {block: 'mb-3 d-lg-none', content: [
              require('../blocks.04-project/panel/panel.tmpl-specs/01-panel-menu.bemjson').new(
                  'Новости',
                  ['Новости', 'Статьи', 'Курсы валют ЦБ'],
                  true
              ),
            ]},
            {tag: 'p', content: 'Наш портал предоставляет самую актуальную и подробную информацию о банковских услугах и последних новостях финансовой отрасли. На нашем сайте вы можете оформить кредит онлайн, оставив заявку сразу во все банки, тем самым сэкономив свое время Чтобы упростить процедуру оформления ссуды воспользуйтесь удобным поиском, сравнением программ, а также кредитным калькулятором.'},
            {block: 'mb-3', content: [
              {block: 'mb-3', content: [
                {block: 'tile-news', cls: 'row', content: new Array(10).fill([
                  {elem: 'col', cls: 'col-12 col-xl-4', content: [
                    require('./common/card-news.bemjson'),
                  ]},
                ])},
              ]},
              // Блок: постраничная навигация
              require('./common/swiper-page-pagination.bemjson'),
            ]},
            {tag: 'p', content: 'Наш портал предоставляет самую актуальную и подробную информацию о банковских услугах и последних новостях финансовой отрасли. На нашем сайте вы можете оформить кредит онлайн, оставив заявку сразу во все банки, тем самым сэкономив свое время. Чтобы упростить процедуру оформления ссуды воспользуйтесь удобным поиском, сравнением программ, а также кредитным калькулятором. У нас вы можете фильтровать банковские предложения по различным параметрам, просмотреть рейтинги, прочитать отзывы и оставить свой. Также вы можете узнать свою кредитную историю и вероятные причины отказа, воспользовавшись сервисом кредитных историй.'},
          ]},
          {block: 'col-12', cls: 'col-lg-5 col-xl-4', content: [
            // Блок: Панель навигации по разделу
            {block: 'mb-4 d-none d-lg-block', content: [
              require('../blocks.04-project/panel/panel.tmpl-specs/01-panel-menu.bemjson').new(
                  'Новости',
                  ['Новости', 'Статьи', 'Курсы валют ЦБ'],
                  false
              ),
            ]},
            // Блок: Банер Вертикальный
            {block: 'sticky-top', mods: {offset: 'top'}, cls: 'h-auto mt-4 zindex-auto', content: [
              require('./common/suggestion-vertical.bemjson'),
            ]},
          ]},
        ]},
        // Блок: Банер широкий
        {block: 'row', cls: 'mb-4', content: [
          {block: 'col-12', content: [
            require('./common/suggestion-horizontal.bemjson'),
          ]},
        ]},
        // Блок: Подписка на рассылку
        {block: 'row', content: [
          {block: 'col-12', content: [
            require('./common/subscribe.bemjson'),
          ]},
        ]},
      ]},
      require('./common/sidebar.bemjson'),
    ]},
    require('./common/footer.bemjson'),
  ],
};
