module.exports = [
    {block: 'tile-news', content: [
        {elem: 'col', cls: 'col-12 col-xl-7 h-xl-100', content: require('./card-news-article-large.bemjson')},
        {elem: 'col', cls: 'col-12 col-xl-5 h-xl-50', content: require('./card-news-article.bemjson')},
        {elem: 'col', cls: 'col-12 col-xl-5 h-xl-50', content: require('./card-news-article.bemjson')},
    ]},
];
