const ITEMS = [
  'Кредиты наличными',
  'Кредитные карты',
  'Микрозаймы',
  'Автокредиты',
  'Кредиты для бизнеса',
  'Ипотека',
  'Вклады',
  'Дебетовые карты',
  'Расчетные счета',
  'Кредитные истории',
  'Кредитный калькулятор',
  'Автокредитный калькулятор',
  'Ипотечный калькулятор',
  'Калькулятор вкладов',
];
module.exports = [
  {block: 'footer', content: [
    {elem: 'body', content: [
      {mix: {block: 'container'}, content: [
        {elem: 'inner', content: [
          {elem: 'row', cls: 'row', content: [
            {elem: 'col', cls: 'col-12 col-sm-6 col-lg-3 pb-3 order-sm-1', content: [
              {elem: 'row', cls: 'row', content: [
                {elem: 'col', cls: 'col-12 col-sm-10 col-lg-12', content: [
                  {block: 'logo', content: [
                    {block: 'a', content: [
                      {block: 'image', mods: {size: '180x90'}, content: [
                        {block: 'img', mods: {lazy: true}, attrs: {width: 180, height: 90}, src: 'images/logo.svg'},
                      ]},
                    ]},
                  ]},
                  {elem: 'copy', cls: 'mt-l', content: 'При использовании материалов гиперссылка обязательна'},
                ]},
              ]},
              {cls: 'mt-l', content: [
                {block: 'attendance', content: [
                  {elem: 'item', content: [
                    {block: 'img', src: 'http://placehold.it/100x31'},
                  ]},
                  {elem: 'item', content: [
                    {block: 'img', src: 'http://placehold.it/90x31'},
                  ]},
                ]},
              ]},
            ]},
            {elem: 'col', cls: 'col-12 col-lg-6 pb-3 order-lg-2', content: [
              {elem: 'navigation', content: [
                {elem: 'row', cls: 'row', content: [
                  {elem: 'col', cls: 'col-12 col-sm-6', content: ITEMS.slice(0, ITEMS.length > 10 ? Math.ceil(ITEMS.length / 2) : 10).map((item)=>[
                    {block: 'a', cls: 'd-block', content: item},
                  ])},
                  {elem: 'col', cls: 'col-12 col-sm-6', content: ITEMS.slice(ITEMS.length > 10 ? Math.ceil(ITEMS.length / 2) + 1 : 10).map((item)=>[
                    {block: 'a', cls: 'd-block', content: item},
                  ])},
                ]},
              ]},
            ]},
            {elem: 'col', cls: 'col-12 col-sm-6 col-lg-3 pb-3 order-2', content: [
              {block: 'vk-group', cls: 'float-lg-right mx-auto mx-sm-0', attrs: {'data-api': 'https://vk.com/js/api/openapi.js?158', 'data-group': '20003922', 'data-mode': 3}},
            ]},
          ]},
        ]},
      ]},
    ]},
    {elem: 'copyright', content: [
      {mix: {block: 'container'}, content: [
        {block: 'row', content: [
          {block: 'col-12', cls: 'col-md-7 mb-1 mb-md-0', content: '2014 - 2018 Кредит Онлайн — Кредиты на любой вкус'},
          {block: 'col-12', cls: 'col-md-5 text-md-right', content: [
            require('./developer.bemjson'),
          ]},
        ]},
      ]},
    ]},
  ]},
  require('./modals/city-change.bemjson'),
  require('./modals/authorization.bemjson'),
  require('./modals/agreement.bemjson'),
];
