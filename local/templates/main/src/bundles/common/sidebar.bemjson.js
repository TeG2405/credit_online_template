module.exports = [
    {block: 'sidebar', content: [
        {mix: {block: 'sticky-top'}, content: [
            {elem: 'inner', cls: 'py-2', content: [
                {content: []},
                {content: [
                    {elem: 'btn-group', content: [
                        {elem: 'btn', content: [
                            {block: 'fi', mods: {icon: 'heart-o'}, attrs: {'data-count': '2'}},
                        ]},
                        {elem: 'btn', content: [
                            {block: 'fi', mods: {icon: 'layers'}, attrs: {'data-count': '2'}},
                        ]},
                    ]},
                    {elem: 'btn-group', cls: 'mt-2 d-lg-none', content: [
                        {elem: 'btn', attrs: {'data-toggle': 'open', 'href': '#FILTER'}, content: [
                            {block: 'fi', mods: {icon: 'filter'}},
                        ]},
                    ]},
                ]},
                {cls: 'position-relative', content: [
                    {elem: 'page-up', cls: 'fade btn-primary pl-l', content: [
                        {elem: 'icon', cls: 'py-m', content: [
                            {block: 'fi', mods: {icon: 'angle-up'}},
                            {block: 'fi', mods: {icon: 'angle-up'}},
                            {block: 'fi', mods: {icon: 'angle-up'}},
                            {block: 'fi', mods: {icon: 'angle-up'}},
                        ]},
                        {tag: 'span', cls: 'align-middle px-l', content: 'Вверх'},
                    ]},
                ]},
            ]},
        ]},
    ]},
];
