module.exports = [
  {block: 'header', content: [
    {elem: 'toolbar', content: [
      {mix: {block: 'container'}, content: [
        require('./toolbar.bemjson'),
      ]},
    ]},
    {elem: 'body', content: [
      {mix: {block: 'fixed-top-control'}, content: [
        {elem: 'fixed', content: [
          {mix: {block: 'container'}, content: [
            {elem: 'inner', content: [
              {elem: 'row', cls: 'row', content: [
                {elem: 'col', cls: 'col-3 d-lg-none', content: [
                  {elem: 'control', attrs: {'data-toggle': 'open', 'href': '#NAVIGATION'}, content: [
                    {block: 'fi', mods: {icon: 'menu'}},
                  ]},
                ]},
                {elem: 'col', cls: 'col-6 col-lg-2', content: [
                  {block: 'logo', cls: 'text-center', content: [
                    {block: 'a', content: [
                      {block: 'image', mods: {size: '130x60'}, content: [
                        {block: 'img', mods: {lazy: true}, src: 'images/logo.svg'},
                      ]},
                    ]},
                  ]},
                ]},
                {elem: 'col', cls: 'col-3 d-lg-none text-right', content: [
                  {elem: 'control', attrs: {'data-toggle': 'open', 'href': '#SEARCH'}, content: [
                    {block: 'fi', mods: {icon: 'search'}},
                  ]},
                ]},
                {elem: 'col', cls: 'col-12 col-lg-10', content: [
                  require('./navigation.bemjson'),
                ]},
              ]},
              {elem: 'search', attrs: {id: 'SEARCH'}, content: [
                require('./search.bemjson'),
              ]},
            ]},
          ]},
        ]},
      ]},
      {mix: {block: 'container'}, content: [
        {elem: 'row', cls: 'row d-lg-none pb-sm-2', content: [
          {elem: 'col', cls: 'col-12', content: [
            {block: 'row', content: [
              {block: 'col-12', cls: 'col-sm-6 px-0 px-sm-2', content: [
                {block: 'btn', cls: 'btn-secondary btn-sm btn-block', content: 'Онлайн-заявки во все банки'},
              ]},
              {block: 'col-12', cls: 'col-sm-6 px-0 px-sm-2', content: [
                {block: 'btn', cls: 'btn-primary btn-sm btn-block', content: [
                  {elem: 'pulsar'},
                  'Подбор кредита',
                ]},
              ]},
            ]},
          ]},
        ]},
      ]},
    ]},
  ]},
];
