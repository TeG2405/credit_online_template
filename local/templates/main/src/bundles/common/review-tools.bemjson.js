module.exports = [
    {block: 'review-tools', content: [
        {cls: 'font-size-small', content: [
            {block: 'btn', cls: 'btn-primary btn-sm mr-m', content: 'Оставить отзыв'},
            {block: 'btn', cls: 'btn-primary btn-sm mr-m', attrs: {'data-toggle': 'modal', 'href': '#MODAL_AUTHORIZATION'}, content: 'Авторизуйтесь'},
            {tag: 'span', cls: 'text-nowrap', content: 'чтобы оставлять комментарии'},
        ]},
    ]},
];
