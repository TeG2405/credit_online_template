module.exports = {
  block: 'page',
  title: 'Предложение - список',
  content: [
    require('./common/header.bemjson'),
    {block: 'main', content: [
      {mix: {block: 'container'}, content: [
        require('./common/breadcrumb.bemjson'),
        {block: 'row', cls: 'mb-4', content: [
          {block: 'col-12', cls: 'col-lg-7 col-xl-8', content: [
            // Главный заголовок
            {block: 'title-compound', content: [
              {elem: 'header', content: 'Кредиты наличными в России'},
              {elem: 'addition', content: [
                require('./common/nav-simple-city.bemjson'),
              ]},
            ]},
            // Блок: Панель навигации по разделу
            {block: 'mb-3 d-lg-none', content: [
              require('../blocks.04-project/panel/panel.tmpl-specs/01-panel-menu.bemjson').new(
                  'Продукты и услуги',
                  [['Кредит наличными', 'hand-money.svg'], ['Микрозайм', 'hand-coins.svg'], ['Кредит для бизнеса', 'briefcase.svg'], ['Кредитные карты', 'credit-card.svg'], ['Автокредит', 'electric-car.svg'], ['Курс валют', 'coins.svg'], ['Дебетовые карты', 'debit-card.svg'], ['Заявка в банки', 'bank.svg'], ['Заявка в МФО', 'message.svg'], ['Ипотека', 'mortgage.svg'], ['Вклады', 'piggy-bank.svg'], ['Кредитный калькулятор', 'calculator.svg']],
                  true
              ),
            ]},
            {block: 'mb-3', content: [
              require('./common/filter.bemjson'),
            ]},
            {block: 'mb-3', cls: 'row', content: [
              {cls: 'col-12', content: [
                {block: 'row', content: [
                  {block: 'col-12', cls: 'col-sm-6 col-xl-4', content: [
                      require('./common/dropdown-sort.bemjson'),
                  ]},
                ]},
              ]},
            ]},
            // Блок: постраничная навигация
            {cls: 'position-relative mb-3', attrs: {id: 'FILTER_LIST'}, content: [
              new Array(10).fill(require('./common/card-offer.bemjson')),
              require('./common/swiper-page-pagination.bemjson'),
            ]},
            {tag: 'p', content: 'Каждый крупный банк предоставляет услуги потребительского кредитования. Из этого вытекает то, что предложений — огромное множество, а выбрать наиболее выгодное решение сложно. Мы предоставим Вам информацию по всем выгодным кредитам наличными в России. Заявка на получение кредита на нашем сайте — приятное и полезное упрощение процесса оформления кредита.'},
            {tag: 'p', content: 'Если Вам по какой-то причине отказали в одном банке, можете заполнить заявку в другой банк. Наилучшим решением вопроса кредитования является заполнение заявок сразу во все банки, так Вы увеличиваете шанс на одобрение, как минимум два банка из десяти одобрят Вам кредит. Вы в свою очередь сможете выбрать из тех банков, что приняли положительное решение наиболее выгодное для Вас кредитное предложение.'},
          ]},
          {block: 'col-12', cls: 'col-lg-5 col-xl-4', content: [
            // Блок: Панель навигации по разделу
            {block: 'mb-4 d-none d-lg-block', content: [
              require('../blocks.04-project/panel/panel.tmpl-specs/01-panel-menu.bemjson').new(
                  'Продукты и услуги',
                  [['Кредит наличными', 'hand-money.svg'], ['Микрозайм', 'hand-coins.svg'], ['Кредит для бизнеса', 'briefcase.svg'], ['Кредитные карты', 'credit-card.svg'], ['Автокредит', 'electric-car.svg'], ['Курс валют', 'coins.svg'], ['Дебетовые карты', 'debit-card.svg'], ['Заявка в банки', 'bank.svg'], ['Заявка в МФО', 'message.svg'], ['Ипотека', 'mortgage.svg'], ['Вклады', 'piggy-bank.svg'], ['Кредитный калькулятор', 'calculator.svg']],
                  false
              ),
            ]},
            // Блок: Банер Вертикальный
            {block: 'sticky-top', mods: {offset: 'top'}, cls: 'h-auto mt-4 zindex-auto', content: [
              require('./common/suggestion-vertical.bemjson'),
            ]},
          ]},
        ]},
        // Блок: Банер широкий
        {block: 'row', cls: 'mb-4', content: [
          {block: 'col-12', content: [
            require('./common/suggestion-horizontal.bemjson'),
          ]},
        ]},
        // Блок: Подписка на рассылку
        {block: 'row', content: [
          {block: 'col-12', content: [
            require('./common/subscribe.bemjson'),
          ]},
        ]},
      ]},
      require('./common/sidebar.bemjson'),
    ]},
    require('./common/footer.bemjson'),
  ],
};
