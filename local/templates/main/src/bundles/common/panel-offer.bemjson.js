const ITEMS = [
    'Микрозаймы',
    'Кредиты',
    'Карты',
];
module.exports = [
    {block: 'panel', cls: 'h-lg-100 bg-light', content: [
        {elem: 'wrap', cls: 'position-lg-absolute', content: [
            {elem: 'heading', cls: 'pb-0', content: [
                {elem: 'title', cls: 'mb-m', content: 'Предложение месяца'},
                {block: 'pb-m', content: [
                    {block: 'nav', mods: {type: 'simple'}, cls: 'justify-content-xl-between', content: [
                        ITEMS.map((item, index)=>[
                            {elem: 'item', cls: 'nav-item', content: [
                                {block: 'a', cls: index ? 'nav-link' : 'nav-link active', attrs: {'data-toggle': 'tab', 'href': '#PANEL_OFFER_TAB_'+index}, content: [
                                    {tag: 'span', content: item},
                                ]},
                            ]},
                        ]),
                    ]},
                ]},
            ]},
            {elem: 'inner', cls: 'tab-content', content: ITEMS.map((item, index)=>[
                {elem: 'wrap', cls: index ? 'tab-pane position-lg-absolute' : 'tab-pane position-lg-absolute active', attrs: {id: 'PANEL_OFFER_TAB_'+index}, content: [
                    {elem: 'body', content: [
                        {elem: 'shadow'},
                        {elem: 'scroll', mix: {block: 'custom-scroll-bar'}, cls: 'position-lg-absolute', content: [
                            {cls: 'px-3', content: [
                                new Array(6).fill(require('./teaser.bemjson')),
                            ]},
                        ]},
                        {elem: 'shadow'},
                    ]},
                    {elem: 'footer', cls: 'font-size-md text-center', content: [
                        {block: 'a', content: 'Все '+item},
                    ]},
                ]},
            ])},
        ]},
    ]},
];
