module.exports = [
    {block: 'swiper-row', cls: 'row', content: [
        {cls: 'w-100', content: [
            {cls: 'swiper-container swiper-container-horizontal', content: [
                {cls: 'swiper-wrapper align-items-stretch', content: ((item) => new Array(12).fill(item))([
                    {block: 'col-12', cls: 'col-md-6 swiper-slide h-auto pt-1', content: [
                        require('./card-special.bemjson'),
                    ]},
                ])},
                {cls: 'swiper-pagination position-relative mt-3'},
            ]},
        ]},
    ]},
];
