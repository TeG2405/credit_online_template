module.exports = [
    {block: 'card-news', content: [
        {elem: 'image', content: [
            {block: 'image', mods: {size: '690x420'}},
        ]},
        {elem: 'body', content: [
            {elem: 'date', content: [
                {elem: 'badge', content: '14:51 Сегодня'},
            ]},
            {elem: 'caption', cls: 'px-2 py-l', content: [
                {elem: 'title', elemMods: {size: 'small'}, content: 'Четверть кредитных карт россиян остается «мертвыми»'},
            ]},
        ]},
    ]},
];
