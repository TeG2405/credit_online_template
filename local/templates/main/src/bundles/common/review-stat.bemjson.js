module.exports = [
    {block: 'review-stat', content: [
        {elem: 'row', cls: 'row align-items-center', content: [
            {elem: 'col', cls: 'col-12 col-sm-6 col-xl-3', content: [
                {elem: 'item', content: [
                    {elem: 'icon', content: [
                        {block: 'fi', cls: 'text-primary', mods: {icon: 'chat'}},
                    ]},
                    {elem: 'body', content: [
                        {elem: 'title', content: 'Всего отзывов'},
                        {elem: 'value', cls: 'text-primary', content: '55'},
                    ]},
                ]},
            ]},
            {elem: 'col', cls: 'col-12 col-sm-6 col-xl-3', content: [
                {elem: 'item', content: [
                    {elem: 'icon', cls: 'text-accent', content: [
                        {block: 'progress-ring', size: '45', width: '4', percent: 4.5/5},
                    ]},
                    {elem: 'body', content: [
                        {elem: 'title', content: 'Рейтинг'},
                        {elem: 'value', cls: 'text-accent', content: '4,5'},
                    ]},
                ]},
            ]},
            {elem: 'col', cls: 'col-12 col-xl-6', content: [
                {cls: 'font-size-small', content: [
                    {block: 'btn', cls: 'btn-primary btn-sm mr-m', attrs: {'data-toggle': 'modal', 'href': '#MODAL_AUTHORIZATION'}, content: 'Авторизуйтесь'},
                    {tag: 'span', cls: 'text-nowrap', content: 'чтобы оставлять комментарии'},
                ]},
            ]},
        ]},
    ]},
];
