module.exports = [
    {block: 'panel', cls: 'bg-light', content: [
        {elem: 'wrap', content: [
            {elem: 'heading', content: [
                {elem: 'title', cls: 'h2 my-0', content: [
                    'Курс валют',
                    {block: 'badge', cls: 'badge-primary badge-pill font-size-base align-middle', content: '01.12.18'},
                ]},
            ]},
            {elem: 'inner', content: [
                {elem: 'wrap', content: [
                    {elem: 'body', content: [
                        {elem: 'scroll', content: [
                            {cls: 'px-3', content: [
                                require('./table-exchange.bemjson'),
                                {tag: 'hr', cls: 'my-0 mt-l'},
                            ]},
                        ]},
                    ]},
                    {elem: 'footer', cls: 'font-size-md pt-l', content: [
                        {block: 'a', content: 'Все курсы'},
                    ]},
                ]},
            ]},
        ]},
    ]},
];
