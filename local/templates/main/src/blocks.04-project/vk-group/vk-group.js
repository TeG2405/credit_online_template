if (NODE_ENV != 'development') {
    Array.from(document.getElementsByClassName('vk-group')).forEach((block)=>{
        window.loader(block.dataset['api'],
            ()=>{
                VK.Widgets.Group(block.id, {mode: block.dataset['mode']}, block.dataset['group']);
            },
            ()=>{
                console.warn('Не удалось загрузить '+ block.dataset['api']);
            }
        );
    });
} else {
    Array.from(document.getElementsByClassName('vk-group')).forEach((block)=>{
        block.innerHTML = '<img class="img" src="http://placehold.it/200x200"/>';
    });
}
