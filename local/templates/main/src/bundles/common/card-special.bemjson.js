module.exports = [
    {block: 'card-special', content: [
        {elem: 'image', content: [
            {block: 'image', mods: {size: '690x380'}},
        ]},
        {elem: 'body', content: [
            {elem: 'caption', content: [
                {cls: 'font-size-small', content: 'Ситибанк'},
                {cls: 'font-size-large text-primary', content: 'Кредитная карта CASH BACK'},
                {block: 'h', size: 3, cls: 'font-weight-light mt-l mb-m', content: 'Дополнительный сash back'},
                {elem: 'table', tag: 'table', content: [
                    {tag: 'tr', content: [
                        {tag: 'td', cls: 'pr-3', content: '1-й год <br> обслуживания'},
                        {tag: 'th', cls: 'font-size-h6 font-weight-normal', content: '0 руб.'},
                    ]},
                    {tag: 'tr', content: [
                        {tag: 'td', cls: 'pr-3', content: 'Ставка'},
                        {tag: 'th', cls: 'font-size-h6 font-weight-normal', content: 'от 16,9%'},
                    ]},
                ]},
                {elem: 'control', content: [
                    {block: 'btn', tag: 'div', cls: 'btn-block btn-primary', content: 'Подробнее'},
                ]},
            ]},
        ]},
    ]},
];


