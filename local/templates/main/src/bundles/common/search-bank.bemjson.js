module.exports = [
    {block: 'row', cls: 'mb-3', content: [
        {block: 'col-12', cls: 'col-sm-9 pr-sm-0 mb-2 mb-sm-0', content: [
            {block: 'form-control', attrs: {'data-toggle': 'autocomplite', 'data-url': 'stubs/banks.json', 'data-callback': 'stubFrontCallBack'}, placeholder: 'Введите название банка'},
        ]},
        {block: 'col-12', cls: 'col-sm-3', content: [
            {block: 'btn', cls: 'btn-primary btn-block', content: 'Найти'},
        ]},
    ]},
];
