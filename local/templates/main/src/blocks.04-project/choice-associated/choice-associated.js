import {Util} from 'bootstrap';
(($)=>{
    $(document).on('change', '.choice-associated__list', (event) => {
        if (event.currentTarget.action) {
            $.ajax(event.currentTarget.action, {
                data: $(event.currentTarget).serialize(),
                success: (data)=>{
                    if (Array.isArray(data)) {
                        window.getBH((BH)=>{
                            $(Util.getSelectorFromElement(event.currentTarget)).html(BH.apply(data.map((item)=> [
                                {block: 'choice-associated', elem: 'item', content: [
                                    {block: 'choice-associated', name: item.name, value: item.value, elem: 'control', content: item.value ? item.value : item},
                                ]},
                            ])));
                        });
                    }
                },
            });
        }
    });
})($);
