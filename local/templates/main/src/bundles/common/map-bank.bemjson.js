module.exports = [
    {block: 'map', cls: 'embed-responsive embed-responsive-16by9', content: [
        {elem: 'inner', cls: 'embed-responsive-item', content: [
            {block: 'ymaps', cls: 'h-100', attrs: {'data-placemarks': 'stubs/ymaps-placemark.json'}},
        ]},
    ]},
];