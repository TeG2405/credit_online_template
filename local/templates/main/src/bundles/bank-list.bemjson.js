module.exports = {
  block: 'page',
  title: 'Банки - список',
  content: [
    require('./common/header.bemjson'),
    {block: 'main', content: [
      {mix: {block: 'container'}, content: [
        require('./common/breadcrumb.bemjson'),
        {block: 'row', cls: 'mb-4', content: [
          {block: 'col-12', cls: 'col-lg-7 col-xl-8', content: [
            // Главный заголовок
            {block: 'title-compound', content: [
              {elem: 'header', content: 'Банки России'},
              {elem: 'addition', content: [
                require('./common/nav-simple-city.bemjson'),
              ]},
            ]},
            // Блок: Панель навигации по разделу
            {block: 'mb-3 d-lg-none', content: [
              require('../blocks.04-project/panel/panel.tmpl-specs/01-panel-menu.bemjson').new(
                  'Банки и компании',
                  ['Банки Волгограда', 'Банки', 'Микрофинансовые организации', 'Бюро кредитных историй'],
                  true
              ),
            ]},
            {tag: 'p', content: 'Список банков России с возможностью поиска, отзывами клиентов, подробными сведениями об услугах и продуктах банков.'},
            // Блок: Поиск банки
            require('./common/search-bank.bemjson'),
            {block: 'mb-3', cls: 'row', content: [
              {cls: 'col-12', content: [
                {block: 'row', content: [
                  {block: 'col-12', cls: 'col-sm-6 col-xl-4', content: [
                      require('./common/dropdown-sort.bemjson'),
                  ]},
                  {block: 'col-12', cls: 'col-sm-6 col-xl-8', content: [
                    {block: 'nav', tag: 'nav', mods: {type: 'toggle'}, cls: 'mt-2 mt-sm-0', content: [
                      {block: 'a', attrs: {'data-toggle': 'tab', 'href': '#BANKS_LIST'}, cls: 'nav-item btn active show btn-blue w-100 w-sm-auto', content: [
                        {block: 'fi', cls: 'align-middle mr-1', mods: {icon: 'align-left'}},
                        {tag: 'span', cls: 'align-middle', content: 'Списком'},
                      ]},
                      {block: 'a', attrs: {'data-toggle': 'tab', 'href': '#BANKS_MAP'}, cls: 'nav-item btn btn-blue w-100 w-sm-auto', content: [
                        {block: 'fi', cls: 'align-middle mr-1', mods: {icon: 'mark'}},
                        {tag: 'span', cls: 'align-middle', content: 'На карте'},
                      ]},
                    ]},
                  ]},
                ]},
              ]},
            ]},
            {block: 'tab-content', cls: 'mb-3', content: [
              {block: 'tab-pane', attrs: {id: 'BANKS_LIST'}, cls: 'fade active show', content: [
                new Array(10).fill(require('./common/card-bank.bemjson')),
                // Блок: постраничная навигация
                require('./common/swiper-page-pagination.bemjson'),
              ]},
              {block: 'tab-pane', attrs: {id: 'BANKS_MAP'}, cls: 'fade', content: [
                require('./common/map-bank.bemjson'),
              ]},
            ]},
            {tag: 'p', content: 'Банк — финансово-кредитная организация, производящая разнообразные виды операций с деньгами и ценными бумагами и оказывающая финансовые услуги правительству, юридическим и физическим лицам.'},
          ]},
          {block: 'col-12', cls: 'col-lg-5 col-xl-4', content: [
            // Блок: Панель навигации по разделу
            {block: 'mb-4 d-none d-lg-block', content: [
              require('../blocks.04-project/panel/panel.tmpl-specs/01-panel-menu.bemjson').new(
                  'Банки и компании',
                  ['Банки Волгограда', 'Банки', 'Микрофинансовые организации', 'Бюро кредитных историй'],
                  false
              ),
            ]},
            // Блок: Банер Вертикальный
            {block: 'sticky-top', mods: {offset: 'top'}, cls: 'h-auto mt-4 zindex-auto', content: [
              require('./common/suggestion-vertical.bemjson'),
            ]},
          ]},
        ]},
        // Блок: Банер широкий
        {block: 'row', cls: 'mb-4', content: [
          {block: 'col-12', content: [
            require('./common/suggestion-horizontal.bemjson'),
          ]},
        ]},
        // Блок: Подписка на рассылку
        {block: 'row', content: [
          {block: 'col-12', content: [
            require('./common/subscribe.bemjson'),
          ]},
        ]},
      ]},
      require('./common/sidebar.bemjson'),
    ]},
    require('./common/footer.bemjson'),
  ],
};
