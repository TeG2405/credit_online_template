import Swiper from 'swiper/dist/js/swiper.min';
(() => {
    Array.prototype.forEach.call(document.getElementsByClassName('swiper-row'), (block) => {
        new Swiper(block.getElementsByClassName('swiper-container')[0], {
            slidesPerView: 'auto',
            spaceBetween: 0,
            watchSlidesVisibility: true,
            slidesPerGroup: 1,
            navigation: {
                nextEl: block.getElementsByClassName('swiper-row__button_next'),
                prevEl: block.getElementsByClassName('swiper-row__button_prev'),
            },
            pagination: {
                el: block.getElementsByClassName('swiper-pagination'),
                type: 'bullets',
                clickable: true,
            },
        });
    });
})();
