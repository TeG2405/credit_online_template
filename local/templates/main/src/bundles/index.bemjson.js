module.exports = {
  block: 'page',
  title: 'Главная странциа',
  content: [
    require('./common/header.bemjson'),
    {block: 'main', content: [
      {mix: {block: 'container'}, content: [
         // Блок: разделы сайта
        {block: 'row', cls: 'mb-4', content: [
          {block: 'col-12', cls: 'col-lg-7 col-xl-8', content: [
            require('./common/tile.bemjson'),
          ]},
          {block: 'col-12', cls: 'col-lg-5 col-xl-4 mt-3 mt-lg-0', content: [
            require('./common/panel-offer.bemjson'),
          ]},
        ]},
        // Блок: Новостей
        {block: 'h', size: 2, cls: 'h1', content: 'Новости'},
        {block: 'row', cls: 'mb-4', content: [
          {block: 'col-12', cls: 'col-md-6 col-lg-7 col-xl-8', content: [
            {block: 'd-flex', cls: 'h-100', content: [
              require('./common/tile-news.bemjson'),
            ]},
          ]},
          {block: 'col-12', cls: 'col-md-6 col-lg-5 col-xl-4 mt-3 mt-md-0', content: [
            require('./common/panel-news.bemjson'),
          ]},
        ]},
        // Блок: Статей
        {block: 'h', size: 2, cls: 'h1', content: 'Статьи'},
        {block: 'row', cls: 'mb-4', content: [
          {block: 'col-12', cls: 'col-md-6 col-lg-7 col-xl-8', content: [
            {block: 'd-flex', cls: 'h-100', content: [
              require('./common/tile-news-article.bemjson'),
            ]},
          ]},
          {block: 'col-12', cls: 'col-md-6 col-lg-5 col-xl-4 mt-3 mt-md-0', content: [
            require('./common/panel-news-article.bemjson'),
          ]},
        ]},
        {block: 'row', cls: 'mb-4', content: [
          {block: 'col-12', cls: 'col-lg-7 col-xl-8', content: [
            // Блок: Рейтинги
            {block: 'h', size: 2, cls: 'h1 mb-3', content: 'Рейтинги'},
            // Блок: Таблица рейтинов
            require('./common/table-rating.bemjson'),
            {block: 'mt-4 d-lg-none', content: [
              // Блок: Курс валют
              require('./common/panel-exchange.bemjson.js'),
            ]},
            // Блок: Карусель предложений
            {block: 'h', size: 2, cls: 'h1', content: 'Специальные предложения'},
            require('./common/swiper-row-special.bemjson'),

            {block: 'h', size: 2, cls: 'h1', content: 'Все банки'},
            // Блок: Поиск банки
            require('./common/search-bank-large.bemjson'),
            // Блок: Карусель банки
            require('./common/swiper-row-bank.bemjson'),
            // Блок: SEO
            {block: 'h', size: 2, cls: 'h1', content: 'Кредит онлайн - это удобно и просто вместе с нами!'},
            require('./common/columns.bemjson'),
          ]},
          {block: 'col-12', cls: 'col-lg-5 col-xl-4', content: [
            // Блок: Курс валют
            {block: 'mb-4 d-none d-lg-block', content: [
              require('./common/panel-exchange.bemjson.js'),
            ]},
            // Блок: Банер Вертикальный
            {block: 'sticky-top', mods: {offset: 'top'}, cls: 'h-auto  mt-4 zindex-auto', content: [
              require('./common/suggestion-vertical.bemjson'),
            ]},
          ]},
        ]},
        // Блок: Банер широкий
        {block: 'row', cls: 'mb-4', content: [
          {block: 'col-12', content: [
            require('./common/suggestion-horizontal.bemjson'),
          ]},
        ]},
        // Блок: Подписка на рассылку
        {block: 'row', content: [
          {block: 'col-12', content: [
            require('./common/subscribe.bemjson'),
          ]},
        ]},
      ]},
      require('./common/sidebar.bemjson'),
    ]},
    require('./common/footer.bemjson'),
  ],
};
