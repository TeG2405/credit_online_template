module.exports = [
    {block: 'suggestion', mods: {align: 'vertical'}, cls: 'bg-light', content: [
        {elem: 'inner', content: [
            {block: 'image', mods: {size: '240x400', align: 'middle'}},
        ]},
    ]},
];
