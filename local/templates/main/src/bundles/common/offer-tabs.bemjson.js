const ITEMS = [
    'Описание',
    'Условия и ставки',
    'Требования и документы',
    'Выдача и погашение',
];
module.exports = [
    {block: 'nav', mods: {type: 'simple'}, cls: 'mb-2', content: [
        ITEMS.map((item, index)=>[
            {elem: 'item', cls: 'nav-item', content: [
                {block: 'a', cls: !index ? 'nav-link active' : 'nav-link', attrs: {'data-toggle': 'tab', 'href': '#OFFER_DETAIL_TAB_'+index}, content: [
                    {tag: 'span', content: item},
                ]},
            ]},
        ]),
        {elem: 'item', cls: 'nav-item', content: [
            {block: 'a', cls: 'nav-link', attrs: {'data-toggle': 'tab', 'href': '#OFFER_DETAIL_TAB_REVIEW'}, content: [
                {tag: 'span', content: 'Отзывы'},
            ]},
        ]},
    ]},
    {block: 'tab-content', content: [
        ITEMS.map((item, index)=>[
            {mix: {block: 'tab-pane'}, cls: index ? '' : 'active', attrs: {id: 'OFFER_DETAIL_TAB_'+index}, content: [
                {block: 'h', size: 4, cls: 'font-weight-normal', content: item},
                {tag: 'p', content: 'Ренесcанс Кредит — потребительские кредиты. Больше не нужно откладывать жизнь на потом! Специально для Вас мы создали кредит, который позволит воплотить мечты в жизнь легко и просто! Живите сейчас и радуйте себя и Ваших близких!'},
                {tag: 'p', content: 'Ренесcанс Кредит — потребительские кредиты. Больше не нужно откладывать жизнь на потом! Специально для Вас мы создали кредит, который позволит воплотить мечты в жизнь легко и просто! Живите сейчас и радуйте себя и Ваших близких!'},
            ]},
        ]),
        {mix: {block: 'tab-pane'}, attrs: {id: 'OFFER_DETAIL_TAB_REVIEW'}, content: [
            {block: 'h', size: 4, cls: 'font-weight-normal', content: 'Отзывы'},
            {content: [
                {block: 'my-3', content: [
                    require('./review-stat.bemjson'),
                ]},
                new Array(3).fill(require('./review.bemjson')),
                {block: 'my-3', content: [
                    require('./review-tools.bemjson'),
                ]},
                // Блок: постраничная навигация
                require('./swiper-page-pagination.bemjson'),
            ]},
        ]},
    ]},
];
