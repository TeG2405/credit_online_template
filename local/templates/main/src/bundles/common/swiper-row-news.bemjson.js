module.exports = [
    {block: 'swiper-row', cls: 'row', content: [
        {cls: 'w-100', content: [
            {cls: 'swiper-container swiper-container-horizontal', content: [
                {cls: 'swiper-wrapper', content: ((item) => new Array(12).fill(item))([
                    {block: 'col-6', cls: 'col-sm-4 col-md-3 col-lg-6 col-xl-3 swiper-slide', content: [
                        require('./card-news-slide.bemjson'),
                    ]},
                ])},
                {cls: 'pt-2'},
                {cls: 'swiper-pagination position-relative mt-2 d-none d-md-block'},
            ]},
        ]},
    ]},
];
