module.exports = [
    {block: 'card-bank', content: [
        {cls: 'row', content: [
            {elem: 'wrap', cls: 'col-12 col-md-3 col-lg-12 col-xl-3', content: [
                {cls: 'row align-items-center', content: [
                    {cls: 'col-6 col-sm-4 col-md-12 col-lg-4 col-xl-12', content: [
                        {elem: 'image', content: [
                            {block: 'a', cls: 'd-block', content: [
                                {block: 'image', mods: {size: '140x50'}},
                            ]},
                        ]},
                    ]},
                    {elem: 'rating', cls: 'col-6 col-sm-8 col-md-12 col-lg-8 col-xl-12', content: [
                        {block: 'rating'},
                    ]},
                ]},
            ]},
            {cls: 'col-12 col-md-9 col-lg-12 col-xl-9', content: [
                {elem: 'title', content: 'Абсолют Банк'},
                {elem: 'body', content: [
                    {cls: 'row', content: [
                        {cls: 'col-12', content: [
                            {elem: 'feature', content: 'Лицензия:  №1000'},
                            {elem: 'feature', content: [
                                {block: 'a', content: 'Отзывы:  2345'},
                            ]},
                        ]},
                    ]},
                ]},
            ]},
        ]},
        {elem: 'info', content: [
            {elem: 'body', content: [
                {elem: 'group', content: [
                    {cls: 'row', content: [
                        {cls: 'col-12 col-md-3 col-lg-12 col-xl-3 font-weight-normal', content: 'Руководитель'},
                        {cls: 'col-12 col-md-9 col-lg-12 col-xl-9', content: 'Костин Андрей Леонидович'},
                    ]},
                ]},
                {elem: 'group', content: [
                    {cls: 'row', content: [
                        {cls: 'col-12 col-md-3 col-lg-12 col-xl-3 font-weight-normal', content: 'Адрес'},
                        {cls: 'col-12 col-md-9 col-lg-12 col-xl-9', content: '190000, г. Санкт-Петербург, ул. Большая Морская, д. 29'},
                    ]},
                ]},
                {elem: 'group', content: [
                    {cls: 'row', content: [
                        {cls: 'col-12 col-md-3 col-lg-12 col-xl-3 font-weight-normal', content: 'Телефоны'},
                        {cls: 'col-12 col-md-9 col-lg-12 col-xl-9', content: [
                            {content: [
                                {block: 'a', href: 'tel:88001002424', content: '8 800 100-24-24'},
                                ' — частным клиентам',
                            ]},
                            {content: [
                                {block: 'a', href: 'tel:88002007799', content: '8 800 200-77-99'},
                                ' — юридическим лицам',
                            ]},
                        ]},
                    ]},
                ]},
                {elem: 'group', content: [
                    {cls: 'row', content: [
                        {cls: 'col-12 col-md-3 col-lg-12 col-xl-3 font-weight-normal', content: 'E-mail'},
                        {cls: 'col-12 col-md-9 col-lg-12 col-xl-9', content: [
                            {block: 'a', href: 'mailto:info@vtb.ru', content: 'info@vtb.ru'},
                        ]},
                    ]},
                ]},
                {elem: 'group', content: [
                    {cls: 'row', content: [
                        {cls: 'col-12 col-md-3 col-lg-12 col-xl-3 font-weight-normal', content: 'Официальный сайт'},
                        {cls: 'col-12 col-md-9 col-lg-12 col-xl-9', content: [
                            {block: 'a', content: 'vtb.ru'},
                        ]},
                    ]},
                ]},
            ]},
        ]},
        {elem: 'row', cls: 'row', content: [
            {elem: 'col', cls: 'col-12 col-xl-5', content: [
                {cls: 'mb-s', content: 'Активы'},
                {cls: 'my-0 h4', content: '12 468 025 960'},
                {cls: 'font-size-md', content: 'тыс. руб.'},
            ]},
            {elem: 'col', cls: 'col-12 col-sm-6 col-xl-3', content: [
                {cls: 'mb-s', content: 'Рейтинг АКРА'},
                {cls: 'my-0 h4', content: [
                    {tag: 'span', cls: 'align-middle', content: 'BB+(RU)'},
                    {block: 'fi', mods: {icon: 'info-circle'}, attrs: {'data-toggle': 'popover', 'data-placement': 'top', 'data-trigger': 'hover', 'data-content': 'Высокий кредитный риск'}, cls: 'text-primary pl-s align-middle'},
                ]},
                {cls: 'font-size-md', content: 'стабильный'},
            ]},
            {elem: 'col', cls: 'col-12 col-sm-6 col-xl-3', content: [
                {cls: 'mb-s', content: 'Рейтинг Moody’s'},
                {cls: 'my-0 h4', content: [
                    {tag: 'span', cls: 'align-middle', content: 'B2'},
                    {block: 'fi', mods: {icon: 'info-circle'}, attrs: {'data-toggle': 'popover', 'data-placement': 'top', 'data-trigger': 'hover', 'data-content': 'Высокий кредитный риск'}, cls: 'text-primary pl-s align-middle'},
                ]},
                {cls: 'font-size-md', content: 'стабильный'},
            ]},
        ]},
    ]},
];
