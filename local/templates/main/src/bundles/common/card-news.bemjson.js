module.exports = [
    {block: 'card-news', content: [
        {elem: 'image', content: [
            {block: 'image', mods: {size: '690x420'}},
        ]},
        {elem: 'body', content: [
            {elem: 'date', content: [
                {elem: 'badge', content: '14:51 Сегодня'},
            ]},
            {elem: 'caption', content: [
                {elem: 'title', content: 'Четверть кредитных карт россиян остается «мертвыми»'},
                {elem: 'description', cls: 'd-xl-none', content: [
                    {tag: 'p', content: 'Совет директоров Банка России на первом заседании в текущем году принял решение понизить ключевую ставку на 25 базисных пунктов — до 7,5% годовых.'},
                    {tag: 'p', content: 'Совет директоров Банка России на первом заседании в текущем году принял решение понизить ключевую ставку на 25 базисных пунктов — до 7,5% годовых.'},
                ]},
            ]},
        ]},
    ]},
];


