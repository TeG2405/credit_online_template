module.exports = function(bh) {
    bh.match('panel__heading', function(ctx, json) {
        if (ctx.attr('data-toggle')) {
            ctx.attr('data-target', '#'+ctx.tParam('ID'));
        }
    });
};
