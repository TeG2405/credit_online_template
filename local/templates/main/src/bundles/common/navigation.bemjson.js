const ITEMS = [
    [['Продукты и услуги'], [
        'Кредиты наличными',
        'Кредитные карты',
        'Микрозаймы',
        'Автокредиты',
        'Кредиты для бизнеса',
        'Ипотека',
        'Вклады',
        'Дебетовые карты',
        'Расчетные счета',
        'Кредитные истории',
        'Кредитный калькулятор',
        'Автокредитный калькулятор',
        'Ипотечный калькулятор',
        'Калькулятор вкладов',
    ]],
    [['Банки и компании'], [
        'Банки волгограда',
        'Банки России',
        'Микрофинансовые организации',
        'Бюро кредитных историй',
    ]],
    [['Новости'], [
        'Новости',
        'Статьи',
        'Курсы валют ЦБ',
    ]],
    [['Рейтинги'], [
        'Народный рейтинг',
        'Финансовые показатели',
        'Кредитные рейтинги',
    ]],
    [['Сервисы'], [
        'Единая заявка на кредит',
        'Единая заявка на займ',
        'Проверка кредитной истории',
        'Подбор кредита',
    ]],
    [['Ещё'], [
        'О проекте',
        'Реклама',
        'Контакты',
        'Пользовательское соглашение',
        'Политика обработки персональных данных',
        'Поиск',
    ]],
];
module.exports = [
    {block: 'navigation', attrs: {id: 'NAVIGATION'}, content: [
        {elem: 'close', content: {block: 'fi', mods: {icon: 'close'}}},
        {elem: 'inner', content: [
            {elem: 'list', content: [
                ITEMS.map((item, index) => [
                    {elem: 'item', content: [
                        {elem: 'link', content: [
                            item[0],
                            {elem: 'icon', mix: {block: 'fi', mods: {icon: 'angle-down'}}},
                        ]},
                        {elem: 'dropdown', content: [
                            {elem: 'row', content: [
                                {elem: 'col', content: item[1].slice(0, item[1].length > 10 ? Math.ceil(item[1].length / 2) : 10).map((item)=>[
                                    {block: 'a', cls: 'd-block', content: item},
                                ])},
                                {elem: 'col', content: item[1].slice(item[1].length > 10 ? Math.ceil(item[1].length / 2) + 1 : 10).map((item)=>[
                                    {block: 'a', cls: 'd-block', content: item},
                                ])},
                            ]},
                        ]},
                    ]},
                ]),
                {elem: 'item', content: [
                    {elem: 'control', attrs: {'data-toggle': 'open', 'href': '#SEARCH'}, mix: {block: 'fi', mods: {icon: 'search'}}},
                ]},
            ]},
        ]},
    ]},
];
