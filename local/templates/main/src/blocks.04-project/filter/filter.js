import {Util} from 'bootstrap';
(($)=>{
    const Event = {
        SHOW: 'show.iv.open',
        HIDE: 'hide.iv.open',
        CHANGE: 'change.iv.filter',
        KEYDOWN: 'keydown.iv.filter',
        UPDATE: 'update.iv.filter',
        SUBMIT: 'submit.iv.filter',
    };
    const Selector = {
        BLOCK: '.filter',
        DROP: '.filter__dropdown',
        SUBMIT: '.filter__submit',
        CONTROL: '.filter__control',
    };
    // Функция добавления backdrop;
    var addBackdrop = (target) => {
        let backdrop = document.createElement('div');
        backdrop.classList.add('progress-updated');
        backdrop.classList.add('fade');
        $(target).append(backdrop);
        Util.reflow(backdrop);
        backdrop.classList.add('show');
        return backdrop;
    };
    // Функция удаления backdrop;
    var removeBackdrop = (backdrop) => {
        backdrop.classList.remove('show');
        $(backdrop)
            .one(Util.TRANSITION_END, () => {
                backdrop.remove();
            })
            .emulateTransitionEnd(Util.getTransitionDurationFromElement(backdrop));
    };
    // Генерация события обновления фильтра
    $(Selector.BLOCK).on(`${Event.CHANGE} ${Event.KEYDOWN}`, (event)=>{
        $(event.currentTarget).trigger(new $.Event(Event.UPDATE, {
            delegateEvent: event,
        }));
    });
    // Обработка события обновления фильтра
    $(Selector.BLOCK).on(Event.UPDATE, (event) => {
        $(event.delegateEvent.target)
            .closest(Selector.DROP)
            .find(Selector.CONTROL)
            .append($(event.currentTarget).find(Selector.SUBMIT).removeClass('d-none'));
        // Вместо алерка ajax запрос и блокирование недопустимых input
        alert('Событие обновления фильтра');
    });
    // Обработка события обновления списка
    $(Selector.BLOCK).on(Event.SUBMIT, (event) => {
        let backdrop = addBackdrop(Util.getSelectorFromElement(event.currentTarget));
        // Вместо setTimeout должен быть ajax запрос на обновление списка;
        alert('Событие обновления списка');
        setTimeout(() => {
            removeBackdrop(backdrop);
            $(event.currentTarget)
                .find(Selector.SUBMIT)
                .addClass('d-none');
        }, 1500);
        event.preventDefault();
    });
})($);

